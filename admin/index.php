<?php
/**
* @package		SLASH-CMS
* @version		index.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

ini_set('display_errors',1);
ini_set('display_startup_errors',1);

//include core
include ("../core/slash.php");
$slash = new Slash ();
$slash->show_admin(); //show back office
?> 
