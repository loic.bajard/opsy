<?php
/**
* @package		SLASH-CMS
* @subpackage	sla_news
* @internal     Admin news module
* @version		sla_news_view.php - Version 11.5.31
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sla_news_view extends slaView implements iView{

	
	/**
	 * Show global HTML Header
	 */
	public function header () {
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/interface/js/interface.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/preload/js/preloadCssImages.js'></script> \n";
	} 
	  
	public function l_header () {
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/pager/js/pager.js'></script> \n";
	}
	
	public function f_header () {
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/ajaxupload/js/ajaxupload.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/datepicker/js/datepicker.js'></script> \n"; //date picker
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/datepicker/js/datepicker-fr.js'></script> \n"; //date picker fr
		echo "<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.js'></script> \n";
		echo "<link rel='stylesheet' type='text/css' media='screen' href='../core/plugins/jquery_plugins/ui/themes/roller/css/roller.css' />"; //Theme roller
		echo "<script type='text/javascript' src='../core/plugins/tiny_mce/jquery.tinymce.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php'></script> \n";
	}
	
	
	/**
	 * Show categories list
	 * @param $message message
	 */
	public function show_items($message="") {
		
		echo "<form name='".$this->controller->module_name."_nav_form' method=post action='index.php?mod=".$this->controller->module_name."'>
			  <input type='hidden' id='".$this->controller->module_name."_act' name='".$this->controller->module_name."_act' value=''>
			  <input type='hidden' id='".$this->controller->module_name."_valid' name='".$this->controller->module_name."_valid' value=''>";
		
		//listing hidden fields
		sl_interface::listing_hidden_fields($this->controller->module_name);
		
		echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<table width='100%' cellspacing='0' cellpadding='0' border='0'><tr>
						<td class='sl_mod_title' align='left' width='50%'>
						<img src='modules/".$this->controller->module_name."/views/default/images/".$this->controller->module_name.".png' align='absmiddle' /> 
						".$this->slash->trad_word("NEWS_TITLE");
						
		sl_interface::create_message_zone($message); //Message
						
		echo "</td><td class='sl_mod_control'>";
					
		//control buttons
		sl_interface::create_control_buttons($this->controller->module_name,array('add','edit','publish','unpublish','delete'));		
					
		echo "</td></tr></table>";
		echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'><tr><td class='sl_mod_list'>";
				
		//create states selection
		
		
		$row_states = array(
						array("id"=> "0", "state" => $this->slash->trad_word("NEWS_IN_PROGRESS")), 
						array("id"=> "1","state" => $this->slash->trad_word("NEWS_PAST")));
		
		
		sl_interface::create_categorie_selection($this->controller->module_name,$this->slash->trad_word("CATEGORIE"),$row_states,"state",1);
				
		$this->controller->news->load_items(); // Load articles list
								
		echo "</td></tr></table></td></tr></table></form>";
					
	}


	
	/**
	 * HTML footer
	 */
	public function footer() {
	
		
		echo "
		<script type='text/javascript'> 
 
			$(document).ready(function(){ 
			
			$('#message').css('opacity',1);
			$('#message').stopAll().pause(5000).fadeTo(400,0, function() { $(this).hide(); } );
			
			$.preloadCssImages();
				
		}); 

 		 			
		</script>";
	
	}
	
	
	
	/**
	 * Show article form
	 */
	public function show_form ($id=0,$values=null,$errors=null) {
		
		$title = "<img src='modules/".$this->controller->module_name."/views/default/images/".$this->controller->module_name.".png' align='absmiddle' /> ";
		
		if ($id != 0) {
			$title .= $this->slash->trad_word("NEWS_TITLE")." >>> <span class='sl_mod_undertitle'>".$this->slash->trad_word("EDIT")."</span>";
		} else {
			$title .= $this->slash->trad_word("NEWS_TITLE")." >>> <span class='sl_mod_undertitle'>".$this->slash->trad_word("ADD")."</span>";
		}
		
		$js_3 = "onclick=\"javascript:
						$('#".$this->controller->module_name."_div6').toggle();
						$('#".$this->controller->module_name."_div7').toggle();
						\"";
		
		
		$obj_titles = array($this->slash->trad_word("TITLE"),
							$this->slash->trad_word("DESCRIPTION"),
							"Choix de l'image",
							$this->slash->trad_word("NEWS_PUBLISH_DATE"),
							$this->slash->trad_word("NEWS_PUBLISH_TIME")." <br />( HH:MM )",
							$this->slash->trad_word("NEWS_PERMANENT"),
							$this->slash->trad_word("NEWS_UNPUBLISH_DATE"),
							$this->slash->trad_word("NEWS_UNPUBLISH_TIME")." <br />( HH:MM )",
							$this->slash->trad_word("ACTIVE"));
		$obj_fieds = array("title","content","","datein","timein","","dateout","timeout","enabled");
		$obj_styles = array(
			array("type" => "input","mandatory" => "1"),
			array("type" => "tinymce"),
			array("type" => "attachments_list", 
					"max_upload" => 1, 
					"accept" => "gif|jpg|txt|pdf", 
					"files_dir" => "medias/attachments/sl_news"),
			array("type" => "date"),
			array("type" => "input","mandatory" => "1"),
			array("type" => "checkbox", "events" => $js_3),
			array("type" => "date"),
			array("type" => "input"),
			array("type" => "checkbox")
			);
			
		$code = "";	
			
		if ($id != 0 ) {
			
			$values["timein"] = substr($values["datein"], 11, 5); 
			$values["timeout"] = substr($values["dateout"], 11, 5); 
			$values["datein"] = substr($values["datein"], 0, 10); 
			$values["dateout"] = substr($values["dateout"], 0, 10); 
			
			
			if ($values["dateout"] == "0000-00-00") {
				$code = "<script type='text/javascript'> 
						$(document).ready(function(){ 
							
							document.".$this->controller->module_name."_add_form.".$this->controller->module_name."_obj5.checked = true;
							
							$('#".$this->controller->module_name."_div6').hide();
							$('#".$this->controller->module_name."_div7').hide();		
									
						});
						</script>";
			}
			
		}
			
		
		
		sl_interface::create_show_form($this->controller->module_name,$title,$id,$obj_fieds,$obj_titles,$obj_styles,$values,null,$errors,$code);
		
		
	}
	
	
	
	
	
	/**
	 * Show article form
	 */
	public function show_delete ($id_array) {
		
		echo "	<form name='".$this->controller->module_name."_del_form' method=post action='index.php?mod=".$this->controller->module_name."'>
				<input type='hidden' id='".$this->controller->module_name."_act' name='".$this->controller->module_name."_act' value='delete'>
				<input type='hidden' id='".$this->controller->module_name."_valid' name='".$this->controller->module_name."_valid' value='1'>
				
				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
				<td>
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='sl_mod_title'>
						<img src='modules/".$this->controller->module_name."/views/default/images/".$this->controller->module_name.".png' align='absmiddle' /> 
						".$this->slash->trad_word("NEWS_TITLE")." >>> <span class='sl_mod_undertitle'>".$this->slash->trad_word("DELETE")."</span></td>
					
						<td class='sl_mod_control'>
							<table align='right' width='200'>
							<tr>
								<td align='center' width='50%'>
										
									<a href='javascript:void(0);' class='del_button' onClick=\"javascript:submitForm('".$this->controller->module_name."','del_apply');\"></a>
									".$this->slash->trad_word("DELETE")."</td>
									
								<td align='center' width='50%'>			
									<a href='index.php?mod=".$this->controller->module_name."' class='undo_button'></a>
									".$this->slash->trad_word("BACK")."				
								</td>

							</tr>
							</table>	
						</td>
					</tr>
					</table>
					
					<br />
					<table width='600' cellspacing='0' cellpadding='10' border='0' align='center' style='border:1px solid #333333;'>
					<tr>
						<td align='left'>		
							<table width='600' cellspacing='0' cellpadding='0' border='0' >
							<tr>
								<td align='left' class='sl_mod_field_title' width='50%'>".$this->slash->trad_word("NEWS_DELETE_CONFIRM")." 
								</td>
							</tr>";
							
					if (count ($id_array) != 0) {
						$count=0;		
						foreach ($id_array as $value) {
								
								$article = $this->controller->news->load_item($value);
								
								echo "<tr><td align='left' class='sl_mod_delete_text' width='50%'>
										<input type='checkbox' id='".$this->controller->module_name."_checked[".$count."]' name='".$this->controller->module_name."_checked[".$count."]' value='".$article["id"]."' checked style='display:none;'  />
										".$article["title"]."
										</td>
									</tr>";
								$count++;
						}
						
					}
						echo"	</table>
						</td>
					</tr>
					
					</table>	
								
			</td>
			</tr></table></form>

				";
				
	}
	
	
	
}





?>