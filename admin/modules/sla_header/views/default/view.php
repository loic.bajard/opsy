<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_HEADER
* @internal     Admin header module
* @version		sla_header_view.php - Version 11.5.31
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sla_header_view extends slaView implements iView{

	/**
	* Show title
	*/
	public function title () {
		echo "<title>Admin</title> \n";
	}
	
	/**
	* Show meta data
	*/
	public function metas () {
	
		echo "	<meta http-equiv='Content-Type'  content='text/html; charset=utf-8' />";
	}
	
	
	/**
	* Show script
	*/
	public function scripts () {
		
		echo "<noscript>Slash use JavaScript. If you see this message, you should enable JavaScript on the preferences web browser ! </noscript> \n";	
		echo "<script type='text/javascript' src='../core/plugins/jquery/jquery.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery/jquery.bgiframe.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery/jquery.dimensions.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery/jquery.delegate.js'></script> \n";
		echo "<script type='text/javascript' src='../core/common/javascript/sl_javascript.js'></script> \n";

		// Huum... Mauvaise idée finalement... Trop récent (trololoooloo)
		// echo '<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>';

	}


}





?>