<?php
/**
* @package		SLASH-CMS
* @subpackage	FR MENU CONFIG MODULE LANGUAGES
* @internal     French MENU_CONFIG module translate
* @version		fr.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_MENU_CONFIG
define("MENU_CONFIG_TITLE", "Gestion des menus");
define("MENU_CONFIG_MENUS", "Menus");
define("MENU_CONFIG_LINKS", "Liens");
define("MENU_CONFIG_CONFIG", "Configuration");
define("MENU_CONFIG_CATEGORIES_SELECT", "Album");
define("MENU_CONFIG_IMAGE_FILE_ADD", "Ajouter un fichier image");
define("MENU_CONFIG_IMAGE_FILE_EDIT", "Remplacer l'image par");
define("MENU_CONFIG_IMAGE_NEED_MESSAGE", "Vous devez ajouter une image en cliquant sur parcourir");
define("MENU_CONFIG_EXTENSION_ACCEPT", "Extensions autoris&eacute;");
define("MENU_CONFIG_DELETE_CONFIRM","Voulez vous supprimer ces images ?");
define("MENU_CONFIG_OVER_PICTURE_QUOTA","Vous avez d&eacute;pass� le nombre d'images autoris&eacute;es");
define("MENU_CONFIG_PICTURE_SIZE_MIN","Taille minimum d'une image");
define("MENU_CONFIG_PICTURE_SIZE_MAX","Taille maximum d'une image");
define("MENU_CONFIG_PICTURE_WEIGHT_MAX","Poids maximum d'une image");
define("MENU_CONFIG_PICTURE_QUOTA_MAX","Nombre d'images autoris&eacute;e");
define("MENU_CONFIG_PICTURE_CURRENT_QUOTA","Nombre d'images actuelles");
define("MENU_CONFIG_SIZE_OPTIMAL","La taille d'une image doit &ecirc;tre de 410/308 pixels pour un affichage optimal");
define("MENU_CONFIG_LIST_MENUS","Liste des menus disponibles");
?>
