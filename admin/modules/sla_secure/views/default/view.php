<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_SECURE
* @internal     Admin Login module
* @version		view.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sla_secure_view extends slaView implements iView {


	/**
	 * HTML Header 
	 */
	public function header () {
		
		echo "<link rel='stylesheet' type='text/css' href='modules/sla_secure/views/default/css/sla_secure.css' media='screen'>\n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/preload/js/preloadCssImages.js'></script> \n";
		echo "<meta http-equiv='Content-Type'  content='text/html; charset=utf-8' />\n";
	}
	
	/**
	 * Login form
	 */
	public function login_form () {
		
		echo "<br />
		<form name='sla_secure_form' method=post action='index.php?mod=sla_secure&act=login'>
			<div class='sla_secure_container'>
				<div class='sla_secure_box'>
					<div class='sla_secure_zn_title connexion_title'>
						<img src='".$this->slash->config["admin_template_url"]."/images/assets/lock.png' width='64' height='64' align='absmiddle'>".$this->slash->trad_word('SECURE_CONNEXION_TEXT')."
					</div>
					<div class='sla_secure_zn_input login_text'>
						".$this->slash->trad_word('SECURE_CONNEXION_LOGIN')." : <input id='sla_secure_login' name='sla_secure_login' type='text' class='login_text'>
					</div>
					<div class='sla_secure_zn_input login_text'>
						".$this->slash->trad_word('SECURE_CONNEXION_PASSWORD')." : <input id='sla_secure_password' name='sla_secure_password' type='password' class='login_text'>
					</div>
					<div class='sla_secure_zn_submit'>
						<input id='sla_secure_submit' name='sla_secure_submit' type='image' src='".$this->slash->config["admin_template_url"]."/images/assets/connexion.png' 
						onMouseOver=\"javascript:this.src='".$this->slash->config["admin_template_url"]."/images/assets/connexion_over.png'\" 
						onMouseOut=\"javascript:this.src='".$this->slash->config["admin_template_url"]."/images/assets/connexion.png'\">
					</div>
					
				</div>
			</div>
		
		
		<!--<table cellspacing='0' cellpadding='0' width='350' border='0' align='center'>
		<tr>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			
			<td>
				<table cellspacing='5' cellpadding='5' width='100%' border='0' align='center' style='border: 1px solid #000000;'>
				<tr>
			        <td align='center'><img src='".$this->slash->config["admin_template_url"]."/images/assets/lock.png' width='64' height='64' align='absmiddle'></td>
					<td class='connexion_title'>".$this->slash->trad_word('SECURE_CONNEXION_TEXT')."</td>
				</tr>
				<tr>
			        <td align='right' class='login_text'>".$this->slash->trad_word('SECURE_CONNEXION_LOGIN')." : </td>
					<td><input id='sla_secure_login' name='sla_secure_login' type='text' class='login_text'></td>
				</tr>
				<tr>
					 <td align='right' class='login_text'>".$this->slash->trad_word('SECURE_CONNEXION_PASSWORD')." : </td>
			        <td><input id='sla_secure_password' name='sla_secure_password' type='password' class='login_text'></td>
				</tr>
				<tr>
					<td align='right' class='login_text'></td>
			        <td><input id='sla_secure_submit' name='sla_secure_submit' type='image' src='".$this->slash->config["admin_template_url"]."/images/assets/connexion.png' 
					onMouseOver=\"javascript:this.src='".$this->slash->config["admin_template_url"]."/images/assets/connexion_over.png'\" 
					onMouseOut=\"javascript:this.src='".$this->slash->config["admin_template_url"]."/images/assets/connexion.png'\"></td>
				</tr>
				</table>
			</td>
			
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		</table>-->
		
		
		
		</form>";
		
	}
	
	/**
	* Show Error
	*/
	public function show_error($message) {
	
		echo "<br />
		<div class='sla_secure_error error_login_text'>
			<img src='".$this->slash->config["admin_template_url"]."/images/assets/error.png' width='16' height='16' align='absmiddle'>&nbsp;".$this->slash->trad_word($message)."
		</div>
		<!--
		<table cellspacing='5' cellpadding='5' width='300' border='0' align='center' style='border: 1px solid #000000;'>
			<tr>
				<td align='left' width='20' class='error_login_text'><img src='".$this->slash->config["admin_template_url"]."/images/assets/error.png' width='16' height='16' align='absmiddle'>&nbsp;</td>
		        <td align='left' class='error_login_text'>".$this->slash->trad_word($message)."</td>
			</tr>
		</table>-->";
	}
	
	
	/**
	 * HTML footer
	 */
	public function footer() {

		echo "<script type='text/javascript'> 
 
			$(document).ready(function(){ 
			
			
				$.preloadCssImages();
				
		}); 			
		</script>";
	
	}
}





?>