 <?php
/**
* @package		SLASH-CMS
* @subpackage	sla_admmenu
* @internal     Admin menu module
* @version		sla_admmenu.php - Version 9.12.16
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

include ("views/default/view.php");

class sla_admmenu_controller extends slaController implements iController{

	public $view;
	
	/**
	* Contructor
	*/
	function sla_construct() {
	   
	   $this->view = new sla_admmenu_view($this);
	}
	
	
	
	/**
	* Initialise function # require by slash-cms #
	*/
	public function initialise() {
		 $this->view->header();
	}
	
	/**
	* Load function # require by slash-cms #
	*/
	public function load() {
		
		$this->view->start_main_menu();
		$this->load_menu(0);
		$this->view->end_main_menu();
		
	}
	
	
	/**
	 * User admin name
	 */
	public function get_admin_username (){
		$row_user = $this->slash->get_admin_infos();
		return $row_user["name"];
	}
	
	
	/**
	* Execute function # require by slash-cms #
	*/
	public function execute() {
		$this->view->execute_menu();
	}

	
	/**
	* Loading menu function
	* @param $parent parent id
	*/
	private function load_menu($parent) {
	
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."admmenu WHERE parent=".$parent." AND enabled=1 ORDER by position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if( mysql_num_rows($result)  > 0 ) {
			
			if ($parent != 0 ) { $this->view->start_under_menu();}
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$this->view->start_menu($row["title_".$_SESSION["user_language"]],$row["type"],$row["action"],$row["icon"]);
				$this->load_menu($row["id"]);
			}
			
			if ($parent != 0 ) { $this->view->end_under_menu();}
			
		} else {
			$this->view->end_menu();
		}
		
		
	}

}



?>
