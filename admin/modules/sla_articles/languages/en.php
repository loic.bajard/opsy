<?php
/**
* @package		SLASH-CMS
* @subpackage	EN ARTICLES MODULE LANGUAGES
* @internal     English articles module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_ARTICLES
define("ARTICLES_TITLE", "Articles config");
define("ARTICLES_DELETE_CONFIRM", "Delete this article ?");
?>
