<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_SLIDER
* @internal     Admin slider module
* @version		slider.php - Version 1
* @author		Alexis CORNELISSENS
* @copyright	Copyright(C) 2013 - Today. All rights reserved.
* @license		GNU/GPL
*/


class slider extends slaModel implements iModel{

	

	public function load_items() {
		
		/* Order */
		$filter = "";
		if ($_SESSION[$this->controller->module_name."_categorie1"] != -1) {
			
			if ($_SESSION[$this->controller->module_name."_categorie1"] == 0) { // en cours
				$filter = "WHERE dateout > '".date("Y-m-d H:i:s")."' ";
			}else{ // pass�e
				$filter = "WHERE dateout < '".date("Y-m-d H:i:s")."' ";
			}
			
		}		
		if ($_SESSION[$this->controller->module_name."_search"] != "#") {
			if ($filter == ""){
				$filter = "WHERE title LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' ";
			}else{
				$filter .= "AND title LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' ";
			}
		}
		
		
		$result = mysql_query("SELECT id,datein,dateout,title,content,id_user,date,enabled 
								FROM ".$this->slash->database_prefix."slider ".$filter."
								ORDER BY ".$_SESSION[$this->controller->module_name."_orderby"]." ".$_SESSION[$this->controller->module_name."_sort"],
		$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		
		$objects = array();
		$obj_ids = array("id","datein","dateout","title","content","id_user","date","enabled");
		$obj_titles = array("ID",
						$this->slash->trad_word("SLIDER_PUBLISH_DATE"),
						$this->slash->trad_word("SLIDER_UNPUBLISH_DATE"),
						$this->slash->trad_word("TITLE"),
						$this->slash->trad_word("CONTENT"),
						$this->slash->trad_word("AUTHOR"),
						$this->slash->trad_word("CREATION_DATE"),
						$this->slash->trad_word("ACTIVE"));
		$obj_sorts = array(false,true,true,true,false,false,true,false);
		$obj_sizes = array(5,10,10,20,20,10,10,5);
		$obj_actions = array(false,"single_edit","single_edit","single_edit","single_edit",false,false,"set_state");
		$obj_controls = array("single_edit","single_delete");

		while ($row = mysql_fetch_array($result, MYSQL_BOTH)) {
			
			/* DATE */
			if ($row[2] == "0000-00-00 00:00:00") {
				$row[2] = "----";
			}
			
			
			/* CONTENT */
			$sl_txt = new sl_text();
			$h2t_content = new html2text($row[4]);
			$row[4] = $sl_txt->substring_word($h2t_content->get_text(),20,true);
			
			/* USER */
			$result_user = mysql_query("SELECT id,name FROM ".$this->slash->database_prefix."users WHERE id='".$row[5]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$row_user = mysql_fetch_array($result_user, MYSQL_ASSOC);
			$row[5] = $row_user["name"];
			
			
			array_push($objects,$row);
		}
		
		//Load listing
		sl_interface::create_listing($this->controller->module_name,$obj_ids,$obj_titles,$obj_sorts,$obj_sizes,$obj_actions,$objects,$obj_controls,true,true,true,true);
		
	}
	
	

	/**
	 * Load categorie
	 * @param $id Categorie ID
	 */
	public function load_item($id) {
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."slider WHERE id=".$id,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		return $row;
	}
	
	
	/**
	 * Delete categorie
	 * @param $id Categorie ID
	 */
	public function delete_items($id_array) {
		foreach ($id_array as $value) {
			if (file_exists("../medias/attachments/sl_slider/".$value)) {
				$this->delete_attachment("../medias/attachments/sl_slider",$value);
			}
			$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."slider WHERE id=".$value,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		}
	}
	
	
	/**
	 * Save categorie
	 */
	public function save_item($id,$values){
		
		if ($id != 0) {
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."slider set 
					id_user='".$_SESSION["id_user"]."',
					datein='".$values["datein"]." ".$values["timein"]."',
					dateout='".$values["dateout"]." ".$values["timeout"]."',
					title='".$values["title"]."',
					content='".$values["content"]."',
					enabled='".$values["enabled"]."' 
					WHERE id='".$values["id"]."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
			return $this->slash->trad_word("EDIT_SUCCESS");	
			
		} else {
		
			$result = mysql_query("INSERT INTO ".$this->slash->database_prefix."slider
					(id,id_user,datein,dateout,title,content,date,enabled) value
					('','".$_SESSION["id_user"]."','".$values["datein"]." ".$values["timein"]."','".$values["dateout"]." ".$values["timeout"]."','".$values["title"]."','".$values["content"]."','".date ("Y-m-d H:i:s", time())."','".$values["enabled"]."')",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
			$insert_id = mysql_insert_id();
					
			$ret = $this->save_attachment("../medias/attachments/sl_slider",$insert_id);
			
			if (!$ret){
				return $this->slash->trad_word("FILE_TRANFERT_FAIL");
			}else{
				return $this->slash->trad_word("SAVE_SUCCESS");
			}					
			
		}
		
	}
	
	/**
	 * Save attachment 
	 */
	public function save_attachment($destination,$id_element){
		
		$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' ORDER BY position",$this->slash->db_handle) 
		or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
		
		if (mysql_num_rows($result_files) > 0) {
		
			$sl_files_sv = new sl_files();
			
			if (!$sl_files_sv->make_dir($destination."/".$id_element)){
				return false;
			}
			
			
			
			while ($row = mysql_fetch_array($result_files, MYSQL_BOTH)) {
				if (!$sl_files_sv->move_files("../tmp/".$row["filename"],$destination."/".$id_element."/".$row["filename"])){
					return false;
				}
			}
			
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."attachments set 
					id_element='".$id_element."',
					state='1'
					WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0'
					",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
			return true;
		}else{
			return true;
		}
	}
	
	/**
	* Delete attachment
	*/
	public function delete_attachment($destination,$id_element){
	
		$sl_files_dl = new sl_files();
		
		if ($sl_files_dl->remove_dir($destination."/".$id_element)) {
			$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."attachments WHERE id_module=".$this->controller->module_id." AND id_element='".$id_element."' and state=1",$this->slash->db_handle) 
									or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			return true;
		}else{
			return false;
		}
				
	}
	
	/**
	 * Set is enabled
	 * @param $id article ID
	 */
	public function set_items_enabled($id_array,$enabled) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."slider set enabled=".$enabled." WHERE id='".$value."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}
	
	
	/**
	* Recovery fields value
	*/
	public function recovery_fields() {
	
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		$obj["title"] = $this->slash->sl_param($this->controller->module_name."_obj0","POST");
		$obj["content"] = $this->slash->sl_param($this->controller->module_name."_obj1","POST");
		$obj["_datein"] = $this->slash->sl_param($this->controller->module_name."_obj3","POST");
		$obj["timein"] = $this->slash->sl_param($this->controller->module_name."_obj4","POST");
		$obj["permanent"] = $this->slash->sl_param($this->controller->module_name."_obj5","POST");
		$obj["_dateout"] = $this->slash->sl_param($this->controller->module_name."_obj6","POST");
		$obj["timeout"] = $this->slash->sl_param($this->controller->module_name."_obj7","POST");
		$obj["enabled"] = $this->slash->sl_param($this->controller->module_name."_obj8","POST");
		
		
		$datein = explode("/",$obj["_datein"]);
		$obj["datein"] = $datein[2]."-".$datein[1]."-".$datein[0];
		
		
		if ($obj["permanent"] == 1) {
			$obj["dateout"] = "0000-00-00 00:00:00";
		}else{
			$dateout = explode("/",$obj["_dateout"]);
			$obj["dateout"] = $dateout[2]."-".$dateout[1]."-".$dateout[0];
		}
		
		return $obj;
		
	}
	
	
	/**
	* Check add/edit values
	* @param $values:Array Object Values
	*/
	public function check_fields($values) {
		
		$mess = array();
		
		$sl_filters = new sl_filters();
		
		if (!$sl_filters->is_time($values["timein"])) {
			$mess[4]["message"] = $this->slash->trad_word("INVALID_FIELD_FORMAT")." ( ex : 09:05 )";
		}
		
		if ($values["permanent"] != 1 ) {
		
			if (!$sl_filters->is_time($values["timeout"])) {
				$mess[7]["message"] = $this->slash->trad_word("INVALID_FIELD_FORMAT")." ( ex : 09:05 )";
			}
			
			if ( !$sl_filters->date_compare($values["datein"],$values["dateout"])) {
				$mess[6]["message"] = $this->slash->trad_word("INVALID_DATE_INTERVAL");
			}
		}
		
		
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	
	}
	
}

?>