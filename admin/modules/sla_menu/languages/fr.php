<?php
/**
* @package		SLASH-CMS
* @subpackage	FR MENU MODULE LANGUAGES
* @internal     French menu module translate
* @version		fr.php - Version 10.4.8
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//Module SLA_MENU
define("SLA_MENU_TITLE", "Gestion des menus");
define("SLA_MENU_DELETE_CONFIRM", "Supprimer ce menu ?");
define("SLA_MENU_ACTION", "Action du lien");
define("SLA_MENU_SELECT_MENU", "Menu");
define("SLA_MENU_LEVEL", "Niveau");
define("SLA_MENU_TOPLEVEL", "Haut");
?>
