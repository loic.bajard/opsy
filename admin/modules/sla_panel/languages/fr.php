<?php
/**
* @package		SLASH-CMS
* @subpackage	FR PANEL MODULE LANGUAGES
* @internal     French panel module translate
* @version		fr.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//Module SLA_PANEL
define("PANEL_TITLE", "Tableau de bord");
define("PANEL_WELCOME", "Bienvenue dans votre interface d'administration");

define("PANEL_SLA_ARTICLE", "Articles");
define("PANEL_SLA_CATEGORIES", "Cat&eacute;gories");
define("PANEL_SLA_MENU", "Menus");
define("PANEL_SLA_NEWS", "Actualit&eacute;");
define("PANEL_SLA_PAGES", "Pages");
define("PANEL_SLA_USERS", "Utilisateurs");
?>
