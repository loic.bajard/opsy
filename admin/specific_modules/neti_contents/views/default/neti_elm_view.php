<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_elm_view
* @internal     Admin netissima module
* @version		neti_elm_view.php - Version 11.3.17
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

class neti_elm_view {


	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	
	/**
	 * Affiche le titre de la page
	 * @title Titre
	 */
	public function show_title($title){
		echo "<div class='neti-page-title'>".$title."</div>";
		
	}
	
	/**
	 * Affiche un paragraphe de type texte (1)
	 * @obj Objet texte
	 */
	public function show_txt($obj){
		if ($obj["txt"]==""){
			echo "<span class='neti-page-text'>&nbsp;</span>";
		}else{
			echo "<span class='neti-page-text'>".$obj["txt"]."</span>";
		}
	}
	
	
	/**
	 * Affiche un paragraphe de type image (2)
	 * @obj Objet image
	 */
	public function show_img($obj){
		
		if (count($obj["images"]) == 1){ //Affiche 1 seul image
			$url = "../medias/attachments/neti_contents/images/".$obj["images"][0]["id_field"]."/".$this->controller->idr."/".$obj["images"][0]["id_element"]."/".$obj["images"][0]["filename"];
			
			if ($obj["img_border"] == 2){
				$class = "neti-img-border";
			}elseif($obj["img_border"] == 3){
				$class = "neti-img-shadow";
			}else{
				$class = "";
			}
			
			if ($obj["img_align"] == 2){
				echo "<div align='center'>";
			}elseif($obj["img_align"] == 3){
				echo "<div align='right'>";
			}else{
				echo "<div align='left'>";
			}
			
				echo "<div class='".$class."'>";
				echo sl_images::show_image($url,$this->controller->neti_config->image_max_width,$this->controller->neti_config->image_max_height);
				echo "</div>";
				
			echo "</div>";
		
		}elseif(count($obj["images"]) > 1) { //Diaporama
			
			echo "<ul id='neti-gallery".$obj["id"]."'>";
			
			for ($i=0;$i<count($obj["images"]);$i++){
				$url = "../medias/attachments/neti_contents/images/".$obj["images"][$i]["id_field"]."/".$this->controller->idr."/".$obj["images"][$i]["id_element"]."/".$obj["images"][$i]["filename"];
				echo "<li><img src='".$url."' /></li>";
			}
			
			echo "</ul>";
			
			
			echo "
			<script type='text/javascript'> 
			
				$(document).ready(function(){
					$('#neti-gallery".$obj["id"]."').galleryView({
						show_filmstrip: false,	 
						panel_width: 600,
	        			panel_height: 400,
	        			panel_scale: 'nocrop',
	        			panel_animation: 'none'				
					});
					
				});
				
			</script>";
			
		}else{ //Pas d'image.
			echo "Aucune image de charg&eacute;";
		}
		
		
		
	}
	
	/**
	 * Affiche un paragraphe de type image + texte (3)
	 * @obj Objet image + texte
	 */
	public function show_imgtxt($obj){
		
		$url = "../medias/attachments/neti_contents/images/".$obj["images"][0]["id_field"]."/".$this->controller->idr."/".$obj["images"][0]["id_element"]."/".$obj["images"][0]["filename"];
			
		if ($obj["img_border"] == 2){
			$class = "neti-img-border";
		}elseif($obj["img_border"] == 3){
			$class = "neti-img-shadow";
		}else{
			$class = "";
		}
		
			
		/*if ($obj["img_align"] == 0 || $obj["img_align"] == 1) {
			echo "<table cellspacing='0' cellpadding='0' border='0'><tr><td width='auto' valign='top'>";
			
			echo "<div class='".$class."'>";
			if (file_exists($url)){
				echo sl_images::show_image($url,floor($this->controller->neti_config->image_max_width/2),floor($this->controller->neti_config->image_max_height/2));
			}else{
				echo "Aucune image de charg&eacute;e";
			}
			echo "</div>";
			
			echo "</td><td width='auto' valign='top' style='padding:5px;'>";
			if ($obj["txt"]==""){
				echo "<span class='neti-page-text'>&nbsp;</span>";
			}else{
				echo "<span class='neti-page-text'>".$obj["txt"]."</span>";
			}
			echo "</td></tr></table>";
		}else{
			echo "<table cellspacing='0' cellpadding='0' border='0'><tr><td width='auto' valign='top' style='padding:5px;'>";
			if ($obj["txt"]==""){
				echo "<span class='neti-page-text'>&nbsp;</span>";
			}else{
				echo "<span class='neti-page-text'>".$obj["txt"]."</span>";
			}
			echo "</td><td width='auto' valign='top'>";
			echo "<div class='".$class."'>";
			if (file_exists($url)){
				echo sl_images::show_image($url,floor($this->controller->neti_config->image_max_width/2),floor($this->controller->neti_config->image_max_height/2));
			}else{
				echo "Aucune image de charg&eacute;e";
			}
			echo "</div>";
			echo "</td></tr></table>";
		}*/
		
		if ($obj["img_align"] == 0 || $obj["img_align"] == 1) {
			echo "<table cellspacing='0' cellpadding='5' border='0' width='100%'><tr><td align='left' valign='top'>";
		
			echo "<div class='".$class."'>";
			echo sl_images::show_image($url);
			echo "</div>";
		
			echo "</td><td  valign='top' style=''  align='left' >";
			if ($obj["txt"]==""){
				echo "<span class='neti-page-text'>&nbsp;</span>";
			}else{
				echo "<span class='neti-page-text'>".$obj["txt"]."</span>";
			}
			echo "</td></tr></table>";
		}else{
			echo "<table cellspacing='0' cellpadding='5' border='0' width='100%'><tr><td  align='left' valign='top' style=''>";
			if ($obj["txt"]==""){
				echo "<span class='neti-page-text'>&nbsp;</span>";
			}else{
				echo "<span class='neti-page-text'>".$obj["txt"]."</span>";
			}
			echo "</td><td  valign='top'  align='left' >";
			echo "<div class='".$class."'>";
			echo sl_images::show_image($url);
			echo "</div>";
			echo "</td></tr></table>";
		}
			

	}
	
	/**
	 * Affiche un paragraphe de type fichier (4)
	 * @obj Objet fichier
	 */
	public function show_file($obj){
		if (count($obj["files"]) == 1){ //Affiche 1 seul image
			
			echo "<div style='
			background: url(\"../templates/mr_promotion/images/fond_telechargement.png\") no-repeat scroll left top transparent;
		  color: #FFFFFF;
		  font-family: verdana,arial;
		  font-size: 12px;
		  height: 54px;
		  text-decoration: none;
		  width: 246px;'> 
		  <div style='margin-left: 18px; padding-top: 10px;'>";
			
			$url = "../medias/attachments/neti_contents/files/".$obj["files"][0]["id_field"]."/".$this->controller->idr."/".$obj["files"][0]["id_element"]."/".$obj["files"][0]["filename"];
			
			$filestools = new sl_files();
			$ext_file = strtolower($filestools->get_file_extension($obj["files"][0]["filename"]));
			
			if (file_exists("../admin/templates/system/images/extensions/16x16/".substr($ext_file, 1).".png")) {
				echo "<img src='templates/system/images/extensions/16x16/".substr($ext_file, 1).".png' align='absmiddle'/>&nbsp;";
			}else{
				echo "<img src='templates/system/images/extensions/16x16/none.png' align='absmiddle'/>&nbsp;";
			}
			
			echo "<a href='".$url."' target='_blank' style='color:#FFFFFF; text-decoration:none; font-size:16px;'>".$obj["title"]."</a>";
			echo "</div></div>";
		}else{ //Pas d'image.
			echo "Aucun fichier";
		}
	}
	
	
	/**
	 * Affiche un paragraphe de type flash (5)
	 * @obj Objet flash
	 */
	public function show_flash($obj){
		echo "Anim flash";
	}
	
	/**
	 * Affiche un paragraphe de type saut de ligne (6)
	 */
	public function show_br(){
		echo "<br/>";
	}
	
	/**
	 * Affichage la barre d'outils de l'elements
	 */
	public function show_elm_actions($id,$lg,$no_edit=false){
		echo "<div id='calque".$lg."_".$id."' class='neti-elm-actions'>";
		
		if ($no_edit == false){
		echo "<a href='index.php?mod=neti_contents&neti_contents_act=edit_elm&ide=".$id."' class='neti-actions_edit' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_BT-ELMEDIT")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a>";
		}
		echo "<a href='index.php?mod=neti_contents&neti_contents_act=up_elm&ide=".$id."' class='neti-actions_up' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_BT-ELMUP")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a>";
		echo "<a href='index.php?mod=neti_contents&neti_contents_act=down_elm&ide=".$id."' class='neti-actions_down' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_BT-ELMDOWN")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a>";
		echo "<a href='javascript:void(0);' onclick=\"if(confirm('".$this->slash->trad_word("NETICONTENTS_TXT-ELMCONFIRMDEL")."')){document.location.href='index.php?mod=neti_contents&neti_contents_act=del_elm&ide=".$id."';}\" class='neti-actions_del' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_BT-ELMDEL")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a>";
		echo "</div>";
	}
	
	
	/**
	 * Formulaires
	 * @param unknown_type $id
	 * @param unknown_type $values
	 * @param unknown_type $error
	 */
	public function show_form($id=0, $values=null, $error=null){
		
		switch($values["type"]){ 
			//Bloc Texte
			case "1": 
				$this->show_form_txt($id,$values,$error);
			break;
			//Bloc Image
			case "2": 
				$this->show_form_img($id,$values,$error);
			break;
			//Bloc Image + Texte
			case "3": 
				$this->show_form_imgtxt($id,$values,$error);
			break;
			//Bloc Fichier
			case "4": 
				$this->show_form_file($id,$values,$error);
			break;
			//Bloc Fichier
			case "5": 
				$this->show_form_flash($id,$values,$error);
			break;
						
			default:
				//Nothing !
				
		}
		
	}
	
	
	/**
	 * Formulaire d'ajout / �dition d'un elements de type texte
	 */
	private function show_form_txt($id=0, $values=null, $error=null) {
		
		$mn = $this->controller->module_name;
		sl_form::start($mn,$id,"save_elm","&type=1");
		
		//Gestion du titre
		
		if ($id != 0) { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-EDITELMTITLE-TXT");
		} else { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-ADDELMTITLE-TXT");
		}
		
		$utitle = $this->slash->trad_word("NETICONTENTS_TL-INRUB").$this->controller->neti_rub->get_title($this->controller->idr,$this->controller->lg[0]['id']);
		
		//Haut de la page
		echo "<div class='neti-container' >";
		$this->controller->neti_core_view->create_header();
		echo "	<div class='neti-content' >";
		
		//Zone de controle
		echo "		<div class='neti-form-top' >";
		$this->controller->neti_core_view->create_title($title,$utitle);
		$this->controller->neti_core_view->create_buttons(array("save","back"));
		echo "		</div>";
		echo "<br/>";
		
		//Onglet Langue + Titres rubriques
		echo "<div class='neti-box-top neti-top-dim-std'><span>".$this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-TXT")."</span>";
		$this->controller->neti_core_view->create_lang_tabs();
		echo "</div>
			<div class='neti-box neti-box-dim-std'>";
		echo "	<div id='neti-tabs-container'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
			
			if($this->controller->lg[$i]["id"] == $this->controller->active_lg){ $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			//if ($i==0) { $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			if (!isset($values["txt-".$this->controller->lg[$i]["id"]])) { $values["txt-".$this->controller->lg[$i]["id"]]=""; }
			echo "<div id='tab_".$this->controller->lg[$i]["id"]."' ".$style.">";
			if ($error["txt-".$this->controller->lg[$i]["id"]]["message"]) { sl_form::error($error["txt-".$this->controller->lg[$i]["id"]]["message"]); echo "<br/><br/>"; }
			$this->controller->neti_view->tinymce($mn,"txt-".$this->controller->lg[$i]["id"],array("value"=>$values["txt-".$this->controller->lg[$i]["id"]], 
							"css" => "../admin/templates/system/css/tinymce.css",
							"theme" => "advanced", 
							"style" => "width:582;height:307px;"));
			
			echo "</div>";
		}
		
		echo "	</div>
			</div>";
		
		echo "</div>";
		
		sl_form::end();
	}
	
	
	/**
	 * Formulaire d'ajout / �dition d'un elements de type image
	 */
	private function show_form_img($id=0, $values=null, $error=null) {
		
		$mn = $this->controller->module_name;
		sl_form::start($mn,$id,"save_elm","&type=2");
		
		//Gestion du titre
		
		if ($id != 0) { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-EDITELMTITLE-IMG");
		} else { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-ADDELMTITLE-IMG");
		}
		
		$utitle = $this->slash->trad_word("NETICONTENTS_TL-INRUB").$this->controller->neti_rub->get_title($this->controller->idr,$this->controller->lg[0]['id']);
		
		//Haut de la page
		echo "<div class='neti-container' >";
		$this->controller->neti_core_view->create_header();
		echo "	<div class='neti-content' >";
		
		//Zone de controle
		echo "		<div class='neti-form-top' >";
		$this->controller->neti_core_view->create_title($title,$utitle);
		$this->controller->neti_core_view->create_buttons(array("save","back"));
		echo "		</div>";
		echo "<br/>";
		
		//Onglet Langue + Titres rubriques
		echo "<div class='neti-box-top neti-top-dim-std'><span>".$this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-IMG")."</span>";
		if ($error["img"]["message"]) { sl_form::error($error["img"]["message"]); }
		$this->controller->neti_core_view->create_lang_tabs();
		echo "</div>
			<div class='neti-box neti-box-dim-std'>";
		echo "	<div id='neti-tabs-container'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
			if($this->controller->lg[$i]["id"] == $this->controller->active_lg){ $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			//if ($i==0) { $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			if ($i==0) { $min_up=1; } else { $min_up=0; }
			echo "<div id='tab_".$this->controller->lg[$i]["id"]."' ".$style.">";

			sl_form::attachments($mn,$this->controller->lg[$i]["id"],array(
										"item_id"=> $id,
										"max_upload"=> 1,
										"files_dir" => "medias/attachments/neti_contents/images/".$this->controller->lg[$i]["id"]."/".$this->controller->idr,
										"accept" => $this->controller->neti_config->authorized_images_extensions));
										
			/*
			sl_form::attachments($mn,$this->controller->lg[$i]["id"],array(
										"item_id"=> $id,
										"min_upload"=> $min_up,
										"files_dir" => "medias/attachments/neti_contents/images/".$this->controller->lg[$i]["id"]."/".$this->controller->idr,
										"accept" => $this->controller->neti_config->authorized_images_extensions));
			*/
			
				/*
			 	"img_resize"=> true,
				"img_max_width"=> $this->controller->neti_config->image_max_width,
				"img_max_height"=> $this->controller->neti_config->image_max_height,
				*/
			
			echo "</div>";
		}
		
		echo "	</div>
			</div>";
		
		//Options et param�tres
		
		/*
		echo "<pre>";
		print_r($values);
		echo "</pre>";
		*/
		
		echo "<br/>

			<div class='neti-box-top neti-top-dim-std'><span>".$this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-OPTIONS")."</span></div>
			<div class='neti-box neti-box-dim-std'>";	

		//Position de l'image
		if (!isset($values["img-".$this->controller->lg[0]["id"]]["img_align"]) || $values["img-".$this->controller->lg[0]["id"]]["img_align"] == 0){ $values["img-".$this->controller->lg[0]["id"]]["img_align"] = 1; }
		
		sl_form::title($this->slash->trad_word("NETICONTENTS_TL-POSITION")." : <br/>");
		echo $this->slash->trad_word("NETICONTENTS_TXT-ELMIMGPOSITION");
		$r_val = array(1,2,3);
		$r_texts = array(
							"<div class='neti-imgalign_left'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
							"<div class='neti-imgalign_center'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
							"<div class='neti-imgalign_right'></div>");
		sl_form::br(2);
		sl_form::radio($mn,"_img_align",array(
									"value" => $values["img-".$this->controller->lg[0]["id"]]["img_align"],
									"values" => $r_val,
									"texts" => $r_texts
									));
		//if ($error["img_align"]["message"]) { sl_form::error($error[2]["message"]); }
		sl_form::br(2);
			
		//Cadre de l'image
		if (!isset($values["img-".$this->controller->lg[0]["id"]]["img_border"]) || $values["img-".$this->controller->lg[0]["id"]]["img_border"]==0){ $values["img-".$this->controller->lg[0]["id"]]["img_border"] = 1; }
		
		sl_form::title($this->slash->trad_word("NETICONTENTS_TL-BORDER")." : <br/>");
		echo $this->slash->trad_word("NETICONTENTS_TXT-ELMIMGBORDER");
		$r_val = array(1,2,3);
		$r_texts = array(
							"<div class='neti-imgborder_none'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
							"<div class='neti-imgborder_solid'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
							"<div class='neti-imgborder_shadow'></div>");
		sl_form::br(2);
		sl_form::radio($mn,"_img_border",array(
									"value" => $values["img-".$this->controller->lg[0]["id"]]["img_border"],
									"values" => $r_val,
									"texts" => $r_texts
									));
		//if ($error[2]["message"]) { sl_form::error($error[2]["message"]); }
		sl_form::br(2);
			
		
		//Action sur l'image
		if (!isset($values["img-".$this->controller->lg[0]["id"]]["img_action"])){ $values["img-".$this->controller->lg[0]["id"]]["img_action"] = 1; }
		if (!isset($values["img-".$this->controller->lg[0]["id"]]["img_url"])){ $values["img-".$this->controller->lg[0]["id"]]["img_url"] = ""; }
		sl_form::title($this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-IMGACTION")." : <br/>");
		echo $this->slash->trad_word("NETICONTENTS_TXT-ELMIMGACTION");
		
		
		
		$field_url = "<input id='".$mn."_obj_img_url' name='".$mn."_obj_img_url' type='text' class='sl_form_input' size='30' value=\"".htmlspecialchars($values["img-".$this->controller->lg[0]["id"]]["img_url"])."\">";
			
			/* R�cup�ration des rubriques */
			$rub = $this->controller->neti_rub->load_all($this->controller->lg[0]["id"]);
			$field_rub = "<select id='".$mn."_obj_img_rub' name='".$mn."_obj_img_rub' class='sl_form_select' >";
						
			for ($i=0; $i<count($rub); $i++) {
				$field_rub .= "<option value='".$rub[$i]["id"]."'";
				if (isset($values["img-".$this->controller->lg[0]["id"]]["img_rub"]) && $rub[$i]["id"] == $values["img-".$this->controller->lg[0]["id"]]["img_rub"]) {
					$field_rub .= "selected";
				}
				$field_rub .= ">".$rub[$i]["title"]."</option>";
			}
							
			$field_rub .= "</select>";
		
		/*$r_val = array(1,2,3,4);
		$r_texts = array(
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-NONE")."<br/><br/>",
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-GOIMG")."<br/><br/>",
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-GOURL")." : ".$field_url."<br/><br/>",
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-GORUB")." : ".$field_rub
						);*/
		$r_val = array(1,2);
		$r_texts = array(
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-NONE")."<br/><br/>",
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-GOIMG")."<br/><br/>"
							
						);			
						
		sl_form::br(2);
		sl_form::radio($mn,"_img_action",array(
									"value" => $values["img-".$this->controller->lg[0]["id"]]["img_action"],
									"values" => $r_val,
									"texts" => $r_texts
									));
		//if ($error[2]["message"]) { sl_form::error($error[2]["message"]); }
		sl_form::br(2);
		
		
		echo "<br/><br/>
			</div>";
		
		
		
		
		//Options Diaporama
		echo "
		<br/>
			<div class='neti-box-top neti-top-dim-std'><span>".$this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-DIAPO")."</span></div>
			<div class='neti-box neti-box-dim-std'>";	

		//A FAIRE
		echo "
			// EN OPTION<br/>
			
	
		";
			
		/*
		 	Largeur du diaporama : XX px<br/>
			Hauteur du diaporama : XX px<br/>
			Miniatures : OUI / NON<br/>
			Position des miniatures : GAUCHE / DROITE / BAS / HAUT<br/>
			Temps d'affichage : XX s<br/>
		*/
		
		echo "
			</div>";
		
		
		
		echo "</div>
		</div>";
		
		sl_form::end();
	}
	
	
	
	
	/**
	 * Formulaire d'ajout / �dition d'un elements de type texte + image
	 */
	private function show_form_imgtxt($id=0, $values=null, $error=null) {
		
		$mn = $this->controller->module_name;
		sl_form::start($mn,$id,"save_elm","&type=3");
		
		//Gestion du titre
		
		if ($id != 0) { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-EDITELMTITLE-IMGTXT");
		} else { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-ADDELMTITLE-IMGTXT");
		}
		
		$utitle = $this->slash->trad_word("NETICONTENTS_TL-INRUB").$this->controller->neti_rub->get_title($this->controller->idr,$this->controller->lg[0]['id']);
		
		//Haut de la page
		echo "<div class='neti-container' >";
		$this->controller->neti_core_view->create_header();
		echo "	<div class='neti-content' >";
		
		//Zone de controle
		echo "		<div class='neti-form-top' >";
		$this->controller->neti_core_view->create_title($title,$utitle);
		$this->controller->neti_core_view->create_buttons(array("save","back"));
		echo "		</div>";
		echo "<br/>";
		
		//Onglet Langue + Titres rubriques
		echo "<div class='neti-box-top neti-top-dim-std'><span>".$this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-IMGTXT")."</span>";
		$this->controller->neti_core_view->create_lang_tabs();
		echo "</div>
			<div class='neti-box neti-box-dim-std'>";
		echo "	<div id='neti-tabs-container'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
			
			if($this->controller->lg[$i]["id"] == $this->controller->active_lg){ $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			//if ($i==0) { $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
						
			//if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]])) { $values["txt-".$this->controller->lg[$i]["id"]]=""; }
			echo "<div id='tab_".$this->controller->lg[$i]["id"]."' ".$style.">";
			
			
			sl_form::attachments($mn,$this->controller->lg[$i]["id"],array(
									"item_id"=> $id,
									"max_upload"=> 20,
									"files_dir" => "medias/attachments/neti_contents/images/".$this->controller->lg[$i]["id"]."/".$this->controller->idr,
									"accept" => $this->controller->neti_config->authorized_images_extensions));
			
			sl_form::br(2);
			
			//if ($error["imgtxt-".$this->controller->lg[$i]["id"]]["message"]) { sl_form::error($error["imgtxt-".$this->controller->lg[$i]["id"]]["message"]); echo "<br/><br/>"; }
			if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"])){ $values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"] = ""; }
			$this->controller->neti_view->tinymce($mn,"txt-".$this->controller->lg[$i]["id"],array("value"=>$values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"], 
							"css" => "templates/system/css/tinymce.css",
							"theme" => "advanced", 
							"style" => "width:582px;height:307px;"));
			
			
			sl_form::br(2);
			
			//Position de l'image
			if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"]) || $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"] == 0){ $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"] = 1; }
			
			sl_form::title($this->slash->trad_word("NETICONTENTS_TL-POSITION")." : <br/>");
			echo $this->slash->trad_word("NETICONTENTS_TXT-ELMIMGPOSITION");
			$r_val = array(1,2);
			$r_texts = array(
								"<div class='neti-imgtxtalign_left'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
								"<div class='neti-imgtxtalign_right'></div>");
			sl_form::br(2);
			sl_form::radio($mn,"_img_align-".$this->controller->lg[$i]["id"],array(
										"value" => $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"],
										"values" => $r_val,
										"texts" => $r_texts
										));
			//if ($error["img_align"]["message"]) { sl_form::error($error[2]["message"]); }
			sl_form::br(2);
			
			//Cadre de l'image
			if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"]) || $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"]==0){ $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"] = 1; }
			
			sl_form::title($this->slash->trad_word("NETICONTENTS_TL-BORDER")." : <br/>");
			echo $this->slash->trad_word("NETICONTENTS_TXT-ELMIMGBORDER");
			$r_val = array(1,2,3);
			$r_texts = array(
								"<div class='neti-imgborder_none'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
								"<div class='neti-imgborder_solid'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
								"<div class='neti-imgborder_shadow'></div>");
			sl_form::br(2);
			sl_form::radio($mn,"_img_border-".$this->controller->lg[$i]["id"],array(
										"value" => $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"],
										"values" => $r_val,
										"texts" => $r_texts
										));
			//if ($error[2]["message"]) { sl_form::error($error[2]["message"]); }
			sl_form::br(2);
				
			
			//Action sur l'image
			
			
			if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]]["img_action"])){ $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_action"] = 1; }
			if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]]["img_url"])){ $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_url"] = ""; }
			sl_form::title($this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-IMGACTION")." : <br/>");
			echo $this->slash->trad_word("NETICONTENTS_TXT-ELMIMGACTION");
			
			
			
			$field_url = "<input id='".$mn."_obj_img_url-".$this->controller->lg[$i]["id"]."' name='".$mn."_obj_img_url-".$this->controller->lg[$i]["id"]."' type='text' class='sl_form_input' size='30' value=\"".htmlspecialchars($values["imgtxt-".$this->controller->lg[$i]["id"]]["img_url"])."\">";
				
				
				$rub = $this->controller->neti_rub->load_all($this->controller->lg[0]["id"]);
				
				$field_rub = "<select id='".$mn."_obj_img_rub' name='".$mn."_obj_img_rub' class='sl_form_select' >";

				for ($j=0; $j<count($rub); $j++) {
					$field_rub .= "<option value='".$rub[$j]["id"]."'";
					
					if (isset($values["imgtxt-".$this->controller->lg[$i]["id"]]["img_rub"]) && $rub[$j]["id"] == $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_rub"]) {
						$field_rub .= "selected";
					}
					$field_rub .= ">".$rub[$j]["title"]."</option>";
				}
								
				$field_rub .= "</select>";
			/*
			$r_val = array(1,2,3,4);
			$r_texts = array(
								$this->slash->trad_word("NETICONTENTS_BT-ACTION-NONE")."<br/><br/>",
								$this->slash->trad_word("NETICONTENTS_BT-ACTION-GOIMG")."<br/><br/>",
								$this->slash->trad_word("NETICONTENTS_BT-ACTION-GOURL")." : ".$field_url."<br/><br/>",
								$this->slash->trad_word("NETICONTENTS_BT-ACTION-GORUB")." : ".$field_rub
							);
			*/
			$r_val = array(1,2);
			$r_texts = array(
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-NONE")."<br/><br/>",
							$this->slash->trad_word("NETICONTENTS_BT-ACTION-GOIMG")."<br/><br/>"
							
						);	
						
			sl_form::br(2);
			sl_form::radio($mn,"_img_action-".$this->controller->lg[$i]["id"],array(
										"value" => $values["imgtxt-".$this->controller->lg[$i]["id"]]["img_action"],
										"values" => $r_val,
										"texts" => $r_texts
										));
			//if ($error[2]["message"]) { sl_form::error($error[2]["message"]); }
			sl_form::br(2);
			
			
			
			
			
			echo "</div>";
		}
		
		echo "	</div>
			</div>";
		
		echo "</div>";
		
		sl_form::end();
	}
	
	
	
	
	
	
	
	
	/**
	 * Formulaire d'ajout / �dition d'un elements de type fichier (4)
	 */
	private function show_form_file($id=0, $values=null, $error=null) {
		
		$mn = $this->controller->module_name;
		sl_form::start($mn,$id,"save_elm","&type=4");
		
		//Gestion du titre
		
		if ($id != 0) { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-EDITELMTITLE-FILE");
		} else { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-ADDELMTITLE-FILE");
		}
		
		$utitle = $this->slash->trad_word("NETICONTENTS_TL-INRUB").$this->controller->neti_rub->get_title($this->controller->idr,$this->controller->lg[0]['id']);
		
		//Haut de la page
		echo "<div class='neti-container' >";
		$this->controller->neti_core_view->create_header();
		echo "	<div class='neti-content' >";
		
		//Zone de controle
		echo "		<div class='neti-form-top' >";
		$this->controller->neti_core_view->create_title($title,$utitle);
		$this->controller->neti_core_view->create_buttons(array("save","back"));
		echo "		</div>";
		echo "<br/>";
		
		//Onglet Langue + Titres rubriques
		echo "<div class='neti-box-top neti-top-dim-std'><span>".$this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-FILE")."</span>";
		$this->controller->neti_core_view->create_lang_tabs();
		echo "</div>
			<div class='neti-box neti-box-dim-std'>";
		echo "	<div id='neti-tabs-container'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
			
			if($this->controller->lg[$i]["id"] == $this->controller->active_lg){ $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			//if ($i==0) { $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
						
			//if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]])) { $values["txt-".$this->controller->lg[$i]["id"]]=""; }
			echo "<div id='tab_".$this->controller->lg[$i]["id"]."' ".$style.">";
			
			
			sl_form::attachments($mn,$this->controller->lg[$i]["id"],array(
									"item_id"=> $id,
									"max_upload"=> 1,
									"files_dir" => "medias/attachments/neti_contents/files/".$this->controller->lg[$i]["id"]."/".$this->controller->idr,
									"accept" => $this->controller->neti_config->authorized_files_extensions));
			
			sl_form::br(2);
			
			//if ($error["imgtxt-".$this->controller->lg[$i]["id"]]["message"]) { sl_form::error($error["imgtxt-".$this->controller->lg[$i]["id"]]["message"]); echo "<br/><br/>"; }
			if (!isset($values["files-".$this->controller->lg[$i]["id"]]["title"])){ $values["files-".$this->controller->lg[$i]["id"]]["title"] = ""; }
			sl_form::title($this->slash->trad_word("NETICONTENTS_TL-FILETITLE")." : ");
			sl_form::input($mn,"title-".$this->controller->lg[$i]["id"],array("value" => $values["files-".$this->controller->lg[$i]["id"]]["title"]));
			
			
			sl_form::br(2);
			

			echo "</div>";
		}
		
		echo "	</div>
			</div>";
		
		echo "</div>";
		
		sl_form::end();
	}
	
	
	
	/**
	 * Formulaire d'ajout / �dition d'un elements de type flash (5)
	 */
	private function show_form_flash($id=0, $values=null, $error=null) {
		
		$mn = $this->controller->module_name;
		sl_form::start($mn,$id,"save_elm","&type=5");
		
		//Gestion du titre
		
		if ($id != 0) { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-EDITELMTITLE-FLASH");
		} else { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-ADDELMTITLE-FLASH");
		}
		
		$utitle = $this->slash->trad_word("NETICONTENTS_TL-INRUB").$this->controller->neti_rub->get_title($this->controller->idr,$this->controller->lg[0]['id']);
		
		//Haut de la page
		echo "<div class='neti-container' >";
		$this->controller->neti_core_view->create_header();
		echo "	<div class='neti-content' >";
		
		//Zone de controle
		echo "		<div class='neti-form-top' >";
		$this->controller->neti_core_view->create_title($title,$utitle);
		$this->controller->neti_core_view->create_buttons(array("save","back"));
		echo "		</div>";
		echo "<br/>";
		
		//Onglet Langue + Titres rubriques
		echo "<div class='neti-box-top neti-top-dim-std'><span>".$this->slash->trad_word("NETICONTENTS_TL-ELMTITLE-FLASH")."</span>";
		$this->controller->neti_core_view->create_lang_tabs();
		echo "</div>
			<div class='neti-box neti-box-dim-std'>";
		echo "	<div id='neti-tabs-container'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
			if($this->controller->lg[$i]["id"] == $this->controller->active_lg){ $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			//if ($i==0) { $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
						
			//if (!isset($values["imgtxt-".$this->controller->lg[$i]["id"]])) { $values["txt-".$this->controller->lg[$i]["id"]]=""; }
			echo "<div id='tab_".$this->controller->lg[$i]["id"]."' ".$style.">";
			
			
			sl_form::attachments($mn,$this->controller->lg[$i]["id"],array(
									"item_id"=> $id,
									"max_upload"=> 1,
									"files_dir" => "medias/attachments/neti_contents/flash/".$this->controller->lg[$i]["id"]."/".$this->controller->idr,
									"accept" => "swf"));
			
			sl_form::br(2);
			
			//if ($error["imgtxt-".$this->controller->lg[$i]["id"]]["message"]) { sl_form::error($error["imgtxt-".$this->controller->lg[$i]["id"]]["message"]); echo "<br/><br/>"; }
			if (!isset($values["flash-".$this->controller->lg[$i]["id"]]["title"])){ $values["flash-".$this->controller->lg[$i]["id"]]["title"] = ""; }
			sl_form::title($this->slash->trad_word("NETICONTENTS_TL-FLASHTITLE")." : ");
			sl_form::input($mn,"title-".$this->controller->lg[$i]["id"],array("value" => $values["flash-".$this->controller->lg[$i]["id"]]["title"]));
			
			sl_form::br(2);
			
			if (!isset($values["flash-".$this->controller->lg[$i]["id"]]["width"])){ $values["flash-".$this->controller->lg[$i]["id"]]["width"] = ""; }
			sl_form::title($this->slash->trad_word("NETICONTENTS_TL-FLASHWIDTH")." : ");
			sl_form::input($mn,"width-".$this->controller->lg[$i]["id"],array("value" => $values["flash-".$this->controller->lg[$i]["id"]]["width"], "size" => "10"));
			echo " px";
			
			sl_form::br(2);
			
			if (!isset($values["flash-".$this->controller->lg[$i]["id"]]["height"])){ $values["flash-".$this->controller->lg[$i]["id"]]["height"] = ""; }
			sl_form::title($this->slash->trad_word("NETICONTENTS_TL-FLASHHEIGHT")." : ");
			sl_form::input($mn,"height-".$this->controller->lg[$i]["id"],array("value" => $values["flash-".$this->controller->lg[$i]["id"]]["height"], "size" => "10" ));
			echo " px";
			
			sl_form::br(2);
			

			echo "</div>";
		}
		
		echo "	</div>
			</div>";
		
		echo "</div>";
		
		sl_form::end();
	}
	
	
}

?>