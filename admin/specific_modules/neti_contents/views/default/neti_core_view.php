<?php 
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_core_view
* @internal     Admin netissima module
* @version		neti_core_view.php - Version 11.3.17
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_core_view {

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}

	/**
	* Actions sur les rubriques
	*/
	public function make_actions(){
	
		echo "<ul>";
		
		//V�rification si autoris� � la cr�ation d'une nouvelle rubrique (voir neti_config->authorized_nb_rub)
		if ($this->controller->neti_rub->get_nb_rub() <= $this->controller->neti_config->authorized_nb_rub ) {
			echo "<li><a href='index.php?mod=neti_contents&neti_contents_act=add_rub&idr=".$this->controller->idr."' class='neti-rub-add'>".$this->slash->trad_word("NETICONTENTS_BT-ADDRUB")."</a></li>";
		}
		
		//Affichage des actions rubriques
		if ($this->controller->idr != "root") {
			echo "<li><a href='index.php?mod=neti_contents&neti_contents_act=edit_rub&idr=".$this->controller->idr."' class='neti-rub-edit'>".$this->slash->trad_word("NETICONTENTS_BT-EDITRUB")."</a></li>";
			//echo "<li><a href='index.php?mod=neti_contents&neti_contents_act=user_rub&idr=".$this->controller->idr."' class='neti-rub-user'>".$this->slash->trad_word("NETICONTENTS_BT-USERRUB")."</a></li>";
			echo "<li><a href='index.php?mod=neti_contents&neti_contents_act=move_rub&idr=".$this->controller->idr."' class='neti-rub-move'>".$this->slash->trad_word("NETICONTENTS_BT-MOVERUB")."</a></li>";
			echo "<li><a href='index.php?mod=neti_contents&neti_contents_act=up_rub&idr=".$this->controller->idr."' class='neti-rub-up'>".$this->slash->trad_word("NETICONTENTS_BT-UPRUB")."</a></li>";
			echo "<li><a href='index.php?mod=neti_contents&neti_contents_act=down_rub&idr=".$this->controller->idr."' class='neti-rub-down'>".$this->slash->trad_word("NETICONTENTS_BT-DOWNRUB")."</a></li>";
			echo "<li><a href='javascript:void(0);' onclick=\"if(confirm('".$this->slash->trad_word("NETICONTENTS_TXT-RUBCONFIRMDEL")."')){document.location.href='index.php?mod=neti_contents&neti_contents_act=del_rub&idr=".$this->controller->idr."';}\" class='neti-rub-del'>".$this->slash->trad_word("NETICONTENTS_BT-DELRUB")."</a></li>";
		}
		echo "</ul>";
	}
	
	/**
	* Barre d'outils
	*/
	public function make_tools () {
		
		if ($this->controller->idr != "root") {
		
		echo "<span class='neti-tools'>
			<table width='100%'><tr>
				<td><a href='index.php?mod=neti_contents&neti_contents_act=add_elm_txt&idr=".$this->controller->idr."' class='neti-tools_txt' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-ADDTXT")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a><br/>".$this->slash->trad_word("NETICONTENTS_BT-TXT")."</td>
					
				<td><a href='index.php?mod=neti_contents&neti_contents_act=add_elm_img&idr=".$this->controller->idr."' class='neti-tools_img'
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-ADDIMG")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a><br/>".$this->slash->trad_word("NETICONTENTS_BT-IMG")."</td>
					
				<td><a href='index.php?mod=neti_contents&neti_contents_act=add_elm_imgtxt&idr=".$this->controller->idr."' class='neti-tools_imgtxt' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-ADDIMGTXT")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a><br/>".$this->slash->trad_word("NETICONTENTS_BT-IMGTXT")."</td>

				<td><a href='index.php?mod=neti_contents&neti_contents_act=add_elm_file&idr=".$this->controller->idr."' class='neti-tools_file' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-ADDFILE")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a><br/>".$this->slash->trad_word("NETICONTENTS_BT-FILE")."</td>
					
				<td><a href='index.php?mod=neti_contents&neti_contents_act=add_elm_flash&idr=".$this->controller->idr."' class='neti-tools_flash'
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-ADDFLASH")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a><br/>".$this->slash->trad_word("NETICONTENTS_BT-FLASH")."</td>

				<td><a href='index.php?mod=neti_contents&neti_contents_act=add_elm_br&idr=".$this->controller->idr."' class='neti-tools_br' 
					onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-ADDBR")."');\"
					onmouseout=\"$('#neti-message').text('');\"></a><br/>".$this->slash->trad_word("NETICONTENTS_BT-BR")."</td>
			</tr></table>
			</span>";
		
		}else{
			echo "<span'>".$this->slash->trad_word("NETICONTENTS_TXT-RUBNEEDSEL")."</span>";
		}
	}
	
	
	/**
	* Affiche le titre de la page
	*/
	public function create_title($title,$utitle="") {
		echo "<div class='neti_modtitle'>".$title;
		if ($utitle){ echo "<br/><span class='neti-utitle'>".$utitle."</span>"; }
		echo "</div>";
	}
	
	
	/**
	* Affiche le haut de page
	*/
	public function create_header() {
		
		/* MAIN MESSAGE */
		if (isset($this->controller->message) && $this->controller->message != ""){
			echo "<div id='message' style='
						position:fixed;
						z-index:999;
						overflow: visible; 
						opacity: 100; 
						display: block;  
						right:0; 
						width:500px; 
						top:0; 
						' >
					<table align='center'>
					<tr><td align='center' bgcolor='F7BE77' width='500' height='46'>".$this->controller->message."</td></tr></table>
				  </div>";
		}
		/* --- */
		
		echo "<div class='neti-head'>
				<span class='neti-title'>".$this->slash->trad_word("NETICONTENTS_TL-TITLE")."</span>
				<div id='neti-msg'><div id='neti-message' class='neti-message'></div></div>
			</div>";
	}
	
	/**
	* Fonctions JavaScript NETIssima
	*/
	public function fct_js(){
	
		echo "<script type='text/javascript'>
				
				function show_tab(id) {
					$('#neti-tabs a').removeClass('neti-tabs-active');";
		for($i=0;$i<count($this->controller->lg);$i++){	
			echo "$('#tab_".$this->controller->lg[$i]["id"]."').hide();";
		}	
		echo "		
					$('#tab_'+id).show();
				}
			
			</script>";
	}
	
	/**
	* Cr�ation des onglets de langue
	*/
	public function create_lang_tabs($current=0){
		
		echo "<div id='neti-tabs' class='neti-tabs'>";

		for($i=0;$i<count($this->controller->lg);$i++){
		
			if($this->controller->lg[$i]["id"] == $this->controller->active_lg){ $class = "class='neti-tabs-active'"; } else { $class = ""; }
			//if ($i==0) { $class = "class='neti-tabs-active'"; } else { $class = ""; }
			
			echo "<a href='javascript:void();' 
			onclick=\"
				show_tab(".$this->controller->lg[$i]["id"].");
				$(this).addClass('neti-tabs-active');
			\" 
			onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-SELECTLANG")." ".$this->controller->lg[$i]["name"]."');\"
			onmouseout=\"$('#neti-message').text('');\" ".$class.">
			<img src='templates/system/images/flags/".$this->controller->lg[$i]["shortname"].".png' width='27' border='0'/></a>";
		}
						
		echo "</div>";
	
	}
	
	/**
	* Cr�ation des onglets de langue pour le preview de la page
	*/
	public function create_lang_tabs_preview($current=0){
		
		echo "<div id='neti-tabs' class='neti-tabs'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
		
			if ($this->controller->active_lg == $this->controller->lg[$i]["id"]) { $class = "class='neti-tabs-active'"; } else { $class = ""; }
			
			echo "<a href='index.php?mod=neti_contents&idr=".$this->controller->idr."&lg=".$this->controller->lg[$i]["id"]."' 
			onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-SELECTLANG")." ".$this->controller->lg[$i]["name"]."');\"
			onmouseout=\"$('#neti-message').text('');\" ".$class.">
			<img src='templates/system/images/flags/".$this->controller->lg[$i]["shortname"].".png' width='27' border='0'/></a>";
		}
						
		echo "</div>";
	
	}
	
	/*
	* Cr�ation des boutons des formulaires
	*/
	public function create_buttons($controls) {
	
		echo "<div class='neti_modcontrols'>";
		echo "<table align='right'><tr>";
		
		for ($i=0;$i<count($controls);$i++) {
			switch($controls[$i]) {
				case "add":
					echo "<td align='center' width='20%'><a href='javascript:void(0);' class='neti-add_button'
									onClick=\"javascript:submitForm('".$this->controller->module_name."','add');\"></a>".$this->slash->trad_word("ADD")."</td>";
				break;
				case "edit":
					echo "<td align='center' width='20%'><a href='javascript:void(0);' class='neti-edit_button'
									onClick=\"javascript:submitForm('".$this->controller->module_name."','edit');\"></a>".$this->slash->trad_word("EDIT")."</td>";	
				break;
				case "publish":
					echo "<td align='center' width='20%'><a href='javascript:void(0);' class='neti-publish_button'
									onClick=\"javascript:submitForm('".$this->controller->module_name."','set_enabled');\"></a>".$this->slash->trad_word("ENABLED")."</td>";
				break;
				case "unpublish":
					echo "<td align='center' width='20%'><a href='javascript:void(0);' class='neti-unpublish_button'
									onClick=\"javascript:submitForm('".$this->controller->module_name."','set_disabled');\"></a>".$this->slash->trad_word("DISABLED")."</td>";
				break;
				case "del":
				case "delete":
					echo "<td align='center' width='20%'><a href='javascript:void(0);' class='neti-delete_button'
									onClick=\"javascript:submitForm('".$this->controller->module_name."','delete');\"></a>".$this->slash->trad_word("DELETE")."</td>";
				break;
				case "save":
					echo "<td align='center' width='20%'><a href='javascript:void(0);' class='neti-apply_button' 
									onClick=\"javascript:submitForm('".$this->controller->module_name."','add_apply');\" 
									onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-SAVE")."');\"
									onmouseout=\"$('#neti-message').text('');\"></a>".$this->slash->trad_word("SAVE")."</td>";
				break;
				case "back":
					echo "<td align='center' width='20%'><a href='index.php?mod=".$this->controller->module_name."' class='neti-undo_button' 
									onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-BACK")."');\"
									onmouseout=\"$('#neti-message').text('');\"></a>
								".$this->slash->trad_word("BACK")."	</td>";
				break;
				
				
				default:
					echo "<td align='center' width='20%'>- NO CONTROL -</td>";
			
			}
		
		}
		
		echo "</tr></table>";
		echo "</div>";		
								
	}

}
?>