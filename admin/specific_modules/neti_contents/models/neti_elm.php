<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin elements functions
* @version		neti_elm.php - Version 11.3.17
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_elm{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	/**
	 * Charge le contenu d'une rubrique si il y a du contenu
	 * @param $idr Rubrique ID
	 * @param $lg Lang ID
	 */
	public function load_page($idr,$lg){
		
		$rub = $this->controller->neti_rub->load($idr);
		
		//Affiche le titre de la page
		if (isset($rub["title-".$lg]) && $rub["title-".$lg]!=""){
			$this->controller->neti_elm_view->show_title($rub["title-".$lg]);
		}else{
			$this->controller->neti_elm_view->show_title($rub["title-".$this->controller->lg[0]['id']]);
		}
		
		$elm = $this->load_elements($idr,$lg);
		
		echo "<ul id='neti-page_".$lg."' class='neti-elm-ul'> \n";
		
		if (count($elm)>0){

			for ($i=0;$i<count($elm);$i++) {
				
				$style = "";
				if ($this->controller->neti_config->dragndrop){ $style = "style='cursor:move;'";}
				echo "<li id='".$elm[$i]["id"]."' class='neti-elm-li' ".$style.">";
				
				echo "<div class='neti-elm_actions-inactive' 
					onmouseover=\"
						$('#calque".$lg."_".$elm[$i]["id"]."').show();
						$(this).removeClass('neti-elm_actions-inactive');
						$(this).addClass('neti-elm_actions-active');
						\" 
					onmouseout=\"
						$('#calque".$lg."_".$elm[$i]["id"]."').hide();
						$(this).removeClass('neti-elm_actions-active');
						$(this).addClass('neti-elm_actions-inactive');
						\">";
				
				if ($elm[$i]["type"] == 6){
					$this->controller->neti_elm_view->show_elm_actions($elm[$i]["id"],$lg,true);
				}else{
					$this->controller->neti_elm_view->show_elm_actions($elm[$i]["id"],$lg);
				}
			
				switch($elm[$i]["type"]) {
					
					//Bloc Texte
					case "1": 
						$obj = array();
						$obj = $this->load_txt($elm[$i]["id"],$lg);
						if ($obj==Null){ $obj = $this->load_txt($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_txt($obj);
					break;
					
					//Bloc Image
					case 2: 
						$obj = array();
						$obj = $this->load_img($elm[$i]["id"],$lg);
						if ($obj["images"]==Null){ $obj = $this->load_img($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_img($obj);
					break;
					
					//Bloc Image + Texte
					case 3:
						$obj = array();
						$obj = $this->load_imgtxt($elm[$i]["id"],$lg);
						$obj_default = $this->load_imgtxt($elm[$i]["id"],$this->controller->lg[0]['id']);
						if ($obj==Null){ $obj = $obj_default; }
						if ($obj["images"] == Null){$obj["images"] = $obj_default["images"];}
						if ($obj["txt"] == ""){$obj["txt"] = $obj_default["txt"];}
						$this->controller->neti_elm_view->show_imgtxt($obj);
					break;
					
					//Bloc Fichier
					case 4:
						$obj = array();
						$obj = $this->load_file($elm[$i]["id"],$lg);
						$obj_default = $this->load_file($elm[$i]["id"],$this->controller->lg[0]['id']);
						if ($obj==Null){ $obj = $obj_default; }
						if ($obj["files"]==Null){ $obj["files"] = $obj_default["files"]; }
						if ($obj["title"]== ""){$obj["title"] = $obj_default["title"];}
						$this->controller->neti_elm_view->show_file($obj);
					break;
					
					//Bloc Flash
					case 5:
						$obj = array();
						$obj = $this->load_flash($elm[$i]["id"],$lg);
						if ($obj["flash"]==Null){ $obj = $this->load_flash($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_flash($obj);
					break;
					
					//Bloc BR
					Case 6:
						$this->controller->neti_elm_view->show_br();
					break;
					
					default:
						// Nothing
				}
				
				echo "</div></li>";
				
			}
			
			echo "</ul>";
			
		}else{
			echo $this->slash->trad_word("NETICONTENTS_TXT-RUBEMPTY");
		}
	}
	
	/**
	 * Charge l'ensemble des éléments dans une page
	 * @param $idr Rubrique ID
	 * @param $lg Lang ID
	 */
	private function load_elements($idr,$lg){
		
	$result = mysql_query("SELECT * FROM neti_elements WHERE id_rub='".$idr."' ORDER BY position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$elm = array();
			$i = 0;	
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$elm[$i] = $row;
				$i++;
			}
			return $elm;
		}else{
			return Null;
		}
	
	}
	
	/**
	 * Charge un élément dans toutes les langues
	 * @param $id ELM ID
	 */
	public function load_element($id){
		$result = mysql_query("SELECT * FROM neti_elements WHERE id='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$obj_elm = array();
			
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			
			$obj_elm["id"] = $row["id"];
			$obj_elm["id_rub"] = $row["id_rub"];
			$obj_elm["type"] = $row["type"];
			$obj_elm["position"] = $row["position"];

			switch($obj_elm["type"]) {
				case "1": //Bloc Texte
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_txt($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["txt-".$this->controller->lg[$i]["id"]] = $obj["txt"];
					}
					break;
				case "2": //Bloc Image
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_img($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["img-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				case "3": //Bloc Image + Texte
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_imgtxt($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["imgtxt-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				case "4": //Bloc Fichier
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_file($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["files-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				case "5": //Bloc Flash
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_flash($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["flash-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				default:
					
					//Nothing
			}
			return $obj_elm;
		}else{
			return Null;
		}
	}
	
	
	/**
	 * Supprime un élément dans toutes les langues
	 * @param $id ELM ID
	 */
	public function delete($id){
		$result = mysql_query("SELECT * FROM neti_elements WHERE id='".$id."'",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			switch($row["type"]) {
					case "1": //Bloc Texte
						$this->delete_txt($id);
						break;
					case 2: //Bloc Image
						$this->delete_img($id);
						break;
					case 3: //Bloc Image + Texte
						$this->delete_imgtxt($id);
						break;
					case 4: //Bloc Fichier
						$this->delete_file($id);
						break;
					case 5: //Bloc Flash
						$this->delete_flash($id);
						break;
					default:
					//Nothing
			}
			mysql_query("DELETE FROM neti_elements WHERE id='".$id."'",$this->slash->db_handle) 
			or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			return $this->slash->trad_word("NETICONTENTS_TXT-ELMDEL-OK");
		}else{
			return $this->slash->trad_word("NETICONTENTS_TXT-ELMDEL-FAIL");
		}
	}
	
	
	/**
	 * Suppression d'un paragraphe de type texte (1)
	 * @param $id Elm ID
	 */
	private function delete_txt($id){
		mysql_query("DELETE FROM neti_txt WHERE id_element='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
	}
	
	/**
	 * Suppression d'un paragraphe de type image (2)
	 * @param $id Elm ID
	 */
	private function delete_img($id){
		
		mysql_query("DELETE FROM neti_img WHERE id_element='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		for($i=0;$i<count($this->controller->lg);$i++){
			$this->delete_attachments($id,$this->controller->lg[$i]["id"],"images");
		}
		
	}
	
	/**
	 * Suppression d'un paragraphe de type image + texte (3)
	 * @param $id Elm ID
	 */
	private function delete_imgtxt($id){
		
		mysql_query("DELETE FROM neti_imgtxt WHERE id_element='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		for($i=0;$i<count($this->controller->lg);$i++){
			$this->delete_attachments($id,$this->controller->lg[$i]["id"],"images");
		}
		
	}
	
	/**
	 * Suppression d'un paragraphe de type file (4)
	 * @param $id Elm ID
	 */
	private function delete_file($id){
		
		mysql_query("DELETE FROM neti_file WHERE id_element='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		for($i=0;$i<count($this->controller->lg);$i++){
			$this->delete_attachments($id,$this->controller->lg[$i]["id"],"files");
		}
		
	}
	
	/**
	 * Suppression d'un paragraphe de type flash (5)
	 * @param $id Elm ID
	 */
	private function delete_flash($id){
		
		mysql_query("DELETE FROM neti_flash WHERE id_element='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		for($i=0;$i<count($this->controller->lg);$i++){
			$this->delete_attachments($id,$this->controller->lg[$i]["id"],"flash");
		}
		
	}
	
	
	/**
	 * Charge un paragraphe de type texte (1)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_txt($id,$lg){
		$result = mysql_query("SELECT * FROM neti_txt WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			return mysql_fetch_array($result, MYSQL_ASSOC);
		}else{
			return Null;	
		}		
	}
	
	/**
	 * Charge un paragraphe de type image (2)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_img($id,$lg){
		$result = mysql_query("SELECT * FROM neti_img WHERE id_element='".$id."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["images"] = $this->load_attachments($id,$lg);	 
			return $arr;
		}else{
			return Null;	
		}				
	}
	
	/**
	 * Charge un paragraphe de type image + texte (3)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_imgtxt($id,$lg){
		$result = mysql_query("SELECT * FROM neti_imgtxt WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["images"] = $this->load_attachments($id,$lg);	 
			return $arr;
		}else{
			return Null;	
		}	
	}
	
	/**
	 * Charge un paragraphe de type fichier (4)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_file($id,$lg){
		$result = mysql_query("SELECT * FROM neti_file WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["files"] = $this->load_attachments($id,$lg);	 
			return $arr;
		}else{
			return Null;	
		}		
	}
	
	
	/**
	 * Charge un paragraphe de type flash (5)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_flash($id,$lg){
		
		$result = mysql_query("SELECT * FROM neti_flash WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["flash"] = $this->load_attachments($id,$lg);	 
			return $arr;
		}else{
			return Null;	
		}		
				
	}
	
	/**
	 * Déplacement d'un paragraphe
	 * @param $id Elm ID
	 * @param $option
	 * 	up : Monter le paragraphe
	 *  down : Descendre le paragraphe
	 */
	public function set_position($id,$option) {
		$result1 = mysql_query("SELECT id, id_rub, position FROM neti_elements WHERE id='".$id."'",$this->slash->db_handle) 
								or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$elm1 = mysql_fetch_array($result1, MYSQL_ASSOC);
		if ($option == "up" || $option == "down") { //Monter ou descendre l'élement
			$order = "ASC";
			if ($option == "up") { $order = "ASC"; }
			if ($option == "down") { $order = "DESC"; }			
			$result_nb = mysql_query("SELECT id, position FROM neti_elements WHERE id_rub='".$elm1['id_rub']."' ORDER BY position ".$order,$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$elm_nb = mysql_fetch_array($result_nb, MYSQL_ASSOC);
			if($id != $elm_nb["id"]){
				$current_pos = $elm1["position"];
				$next_pos = $current_pos - 1;
				if ($option == "up") { $next_pos = $current_pos - 1; }
				if ($option == "down") { $next_pos = $current_pos + 1; }
				$result2 = mysql_query("SELECT id, id_rub, position FROM neti_elements WHERE id_rub='".$elm1['id_rub']."' AND position='".$next_pos."'",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$elm2 = mysql_fetch_array($result2, MYSQL_ASSOC);
				mysql_query("UPDATE neti_elements set 
					position='".$next_pos."'
					WHERE id='".$id."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				mysql_query("UPDATE neti_elements set 
					position='".$current_pos."'
					WHERE id='".$elm2["id"]."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				return $this->slash->trad_word("NETICONTENTS_TXT-ELMMOVE-OK");
			}else{
				return $this->slash->trad_word("NETICONTENTS_TXT-ELMMOVE-FAIL");
			}
		}	
	}
	
	
	
	/**
	 * Enregistrement / modification
	 */
	public function save($id,$values,$type){
		switch($type) {
			case "1": //Bloc Texte
				$ret = $this->save_txt($id,$values);
				break; 
			case "2": //Bloc Image
				$ret = $this->save_img($id,$values);
				break;
			case "3": //Bloc Image + Texte
				$ret = $this->save_imgtxt($id,$values);
				break;
			case "4": //Bloc Fichier
				$ret = $this->save_file($id,$values);
				break;
			case "5": //Bloc Flash
				$ret = $this->save_flash($id,$values);
				break;
			case "6": //Bloc Saut de ligne
				$ret = $this->save_br($id,$values);
				break;
			default:
				//Nothing
		}
		return $ret;
	}
	
	
	/**
	 * Enregistrement / modification Txt
	 */
	public function save_txt($id,$values){
		if ($id != 0) { //Modification
			for($i=0;$i<count($this->controller->lg);$i++){
				$result = mysql_query("SELECT * FROM neti_txt WHERE id_element='".$id."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				if (mysql_num_rows($result) > 0) {
					mysql_query("UPDATE neti_txt set 
						txt='".mysql_real_escape_string($values["txt-".$this->controller->lg[$i]["id"]])."' 
						WHERE id_element='".$values["id"]."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				}else{
					if ($values["txt-".$this->controller->lg[$i]["id"]]) {
						mysql_query("INSERT INTO neti_txt 
							(id,id_element,id_lg,txt) value
							('','".$values["id"]."','".$this->controller->lg[$i]["id"]."','".$values["txt-".$this->controller->lg[$i]["id"]]."')",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					}
				}
			}
			return $this->slash->trad_word("EDIT_SUCCESS");	
		} else { //Ajout 
			$id_elm = $this->add_element(1);						
			if ($id_elm != Null) {
				for($i=0;$i<count($this->controller->lg);$i++){
					if ($values["txt-".$this->controller->lg[$i]["id"]]) {
						mysql_query("INSERT INTO neti_txt 
							(id,id_element,id_lg,txt) value
							('','".$id_elm."','".$this->controller->lg[$i]["id"]."','".$values["txt-".$this->controller->lg[$i]["id"]]."')",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					}
				}
				return $this->slash->trad_word("SAVE_SUCCESS");		
			}else{
				return "FAIL";
			}
		}
	}
	
	
	
	/**
	 * Enregistrement / modification Img
	 */
	public function save_img($id,$values){
		if ($id != 0) { //Modification 
			
			$result = mysql_query("SELECT * FROM neti_img WHERE id_element='".$id."'",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				if (mysql_num_rows($result) > 0) {
					mysql_query("UPDATE neti_img set 
						img_align='".$values["img_align"]."',
						img_border='".$values["img_border"]."',
						img_action='".$values["img_action"]."',
						img_url='".$values["img_url"]."',
						img_rub='".$values["img_rub"]."',
						diapo_width='".$values["diapo_width"]."',
						diapo_height='".$values["diapo_height"]."',
						diapo_thumbs='".$values["diapo_thumbs"]."',
						diapo_thumbs_position='".$values["diapo_thumbs_position"]."',
						diapo_time='".$values["diapo_time"]."' 
						WHERE id_element='".$id."'",$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
						
					return $this->slash->trad_word("EDIT_SUCCESS");	
						
				}else{
					return "FAIL";
				}
			
			
			
		}else{ // Ajout
			
			$id_elm = $this->add_element(2);						
			if ($id_elm != Null) {
				
				mysql_query("INSERT INTO neti_img 
							(id,id_element,
								img_align,
								img_border,
								img_action,
								img_url,
								img_rub,
								diapo_width,
								diapo_height,
								diapo_thumbs,
								diapo_thumbs_position,
								diapo_time
							) value
							('','".$id_elm."',
								'".$values["img_align"]."',
								'".$values["img_border"]."',
								'".$values["img_action"]."',
								'".$values["img_url"]."',
								'".$values["img_rub"]."',
								'".$values["diapo_width"]."',
								'".$values["diapo_height"]."',
								'".$values["diapo_thumbs"]."',
								'".$values["diapo_thumbs_position"]."',
								'".$values["diapo_time"]."'
							)",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
										
							
					
				for($i=0;$i<count($this->controller->lg);$i++){
				
					$ret = $this->save_attachments($id_elm,$this->controller->lg[$i]["id"],"images");
					
					if (!$ret) {
						echo "ECHEC : id langue (".$this->controller->lg[$i]["id"].") id elm(".$id_elm.")";
					} 
				
				}
				
				return $this->slash->trad_word("SAVE_SUCCESS");	
				
			}
		} 
	}
	
	
	/**
	 * Enregistrement / modification Images + Txt
	 */
	public function save_imgtxt($id,$values){
		
		if ($id != 0) { //Modification
			
					
			for($i=0;$i<count($this->controller->lg);$i++){
				$result = mysql_query("SELECT * FROM neti_imgtxt WHERE id_element='".$id."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if (mysql_num_rows($result) > 0) {
					mysql_query("UPDATE neti_imgtxt set 
						txt='".mysql_real_escape_string($values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"])."', 
						img_align='".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"]."', 
						img_border='".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"]."', 
						img_action='".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_action"]."', 
						img_url='".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_url"]."', 
						img_rub='".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_rub"]."' 
						WHERE id_element='".$values["id"]."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				}else{
					if ($values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"]) {
						mysql_query("INSERT INTO neti_imgtxt 
							(id,id_element,id_lg,txt,img_align,img_border,img_action,img_url,img_rub) value
							('','".$values["id"]."','".$this->controller->lg[$i]["id"]."',
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"]."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"]."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"]."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_action"]."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_url"]."', 
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_rub"]."' 
							)",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					}
				}
			}
			
			return $this->slash->trad_word("EDIT_SUCCESS");	
			
		} else { //Ajout
			
			$id_elm = $this->add_element(3);						
			if ($id_elm != Null) {
				
				for($i=0;$i<count($this->controller->lg);$i++){
				
					$ret = $this->save_attachments($id_elm,$this->controller->lg[$i]["id"],"images");
					
					if (!$ret) {
						echo "ECHEC : id langue (".$this->controller->lg[$i]["id"].") id elm(".$id_elm.")";
					}else{
						
						if ($values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"]) {
						mysql_query("INSERT INTO neti_imgtxt 
							(id,id_element,id_lg,txt,img_align,img_border,img_action,img_url,img_rub) value
							('','".$id_elm."','".$this->controller->lg[$i]["id"]."',
							'".mysql_real_escape_string($values["imgtxt-".$this->controller->lg[$i]["id"]]["txt"])."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"]."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"]."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_action"]."',  
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_url"]."', 
							'".$values["imgtxt-".$this->controller->lg[$i]["id"]]["img_rub"]."' 
							)",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
						}
						
					}
				
				}
				
				return $this->slash->trad_word("SAVE_SUCCESS");	
				
			}
		}
	}
	
	
	
	/**
	 * Enregistrement / modification Fichier (4)
	 */
	public function save_file($id,$values){
		
		if ($id != 0) { //Modification
			
					
			for($i=0;$i<count($this->controller->lg);$i++){
				$result = mysql_query("SELECT * FROM neti_file WHERE id_element='".$id."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if (mysql_num_rows($result) > 0) {
					mysql_query("UPDATE neti_file set 
						title='".$values["file-".$this->controller->lg[$i]["id"]]["title"]."' 
						WHERE id_element='".$values["id"]."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				}else{
					if ($values["file-".$this->controller->lg[$i]["id"]]["title"]) {
						mysql_query("INSERT INTO neti_file 
							(id,id_element,id_lg,title) value
							('','".$values["id"]."','".$this->controller->lg[$i]["id"]."',
							'".$values["file-".$this->controller->lg[$i]["id"]]["title"]."' 
							)",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					}
				}
			}
			
			return $this->slash->trad_word("EDIT_SUCCESS");	
			
		} else { //Ajout
			
			$id_elm = $this->add_element(4);						
			if ($id_elm != Null) {
				
				for($i=0;$i<count($this->controller->lg);$i++){
				
					$ret = $this->save_attachments($id_elm,$this->controller->lg[$i]["id"],"files");
					
					if (!$ret) {
						echo "ECHEC : id langue (".$this->controller->lg[$i]["id"].") id elm(".$id_elm.")";
					}else{
						
						if ($values["file-".$this->controller->lg[$i]["id"]]["title"]) {
						mysql_query("INSERT INTO neti_file 
							(id,id_element,id_lg,title) value
							('','".$id_elm."','".$this->controller->lg[$i]["id"]."',
							'".$values["file-".$this->controller->lg[$i]["id"]]["title"]."'   
							)",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
						}
						
					}
				
				}
				
				return $this->slash->trad_word("SAVE_SUCCESS");	
				
			}
		}
	}
	
	
	
	/**
	 * Enregistrement / modification Flash (5)
	 */
	public function save_flash($id,$values){
		
		if ($id != 0) { //Modification
			
					
			for($i=0;$i<count($this->controller->lg);$i++){
				$result = mysql_query("SELECT * FROM neti_flash WHERE id_element='".$id."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if (mysql_num_rows($result) > 0) {
					mysql_query("UPDATE neti_flash set 
						title='".$values["flash-".$this->controller->lg[$i]["id"]]["title"]."', 
						width='".$values["flash-".$this->controller->lg[$i]["id"]]["width"]."',
						height='".$values["flash-".$this->controller->lg[$i]["id"]]["height"]."' 
						WHERE id_element='".$values["id"]."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				}else{
					if ($values["flash-".$this->controller->lg[$i]["id"]]["title"]) {
						mysql_query("INSERT INTO neti_flash 
							(id,id_element,id_lg,title,width,height) value
							('','".$values["id"]."','".$this->controller->lg[$i]["id"]."',
							'".$values["flash-".$this->controller->lg[$i]["id"]]["title"]."',
							'".$values["flash-".$this->controller->lg[$i]["id"]]["width"]."' ,
							'".$values["flash-".$this->controller->lg[$i]["id"]]["height"]."'  
							)",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					}
				}
			}
			
			return $this->slash->trad_word("EDIT_SUCCESS");	
			
		} else { //Ajout
			
			$id_elm = $this->add_element(5);						
			if ($id_elm != Null) {
				
				for($i=0;$i<count($this->controller->lg);$i++){
				
					$ret = $this->save_attachments($id_elm,$this->controller->lg[$i]["id"],"flash");
					
					if (!$ret) {
						echo "ECHEC : id langue (".$this->controller->lg[$i]["id"].") id elm(".$id_elm.")";
					}else{
						
						if ($values["flash-".$this->controller->lg[$i]["id"]]["title"]) {
						mysql_query("INSERT INTO neti_flash 
							(id,id_element,id_lg,title) value
							('','".$id_elm."','".$this->controller->lg[$i]["id"]."',
							'".$values["flash-".$this->controller->lg[$i]["id"]]["title"]."'   
							)",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
						}
						
					}
				
				}
				
				return $this->slash->trad_word("SAVE_SUCCESS");	
				
			}
		}
	}
	
	
	
	
	
	/**
	 * Enregistrement BR
	 */
	public function save_br($id,$values){
		$id_elm = $this->add_element(6);						
		if ($id_elm != Null) {
			return $this->slash->trad_word("SAVE_SUCCESS");	
		}else{
			return "FAIL";
		}
	}
	
	
	/**
	 * Charge un fichier / image jointe
	 */
	private function load_attachments($id_elm,$lg) {
		$id_mod = $this->controller->module_id;
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_element='".$id_elm."' AND state=1 AND id_module='".$id_mod."' AND id_field='".$lg."' ORDER BY position asc",$this->slash->db_handle) 
			or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {	
			$attachs = array();
			$i=0;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){	
				$attachs[$i]=$row;
				$i++;
			}
			return $attachs;
		}else{
			return null;
		}
		
	}
	
	
	/**
	 * Save attachment
	 */
	private function save_attachments($id_element,$lg,$type="images"){
		
		$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' and id_field='".$lg."' ORDER BY position",$this->slash->db_handle) 
		or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
		
		if (mysql_num_rows($result_files) > 0) { //Des fichiers ont été envoyée
		
			$sl_files_sv = new sl_files();
			
			$destination = "../medias/attachments/neti_contents/".$type."/".$lg."/".$this->controller->idr;

			if (!$sl_files_sv->make_dir($destination."/".$id_element,true)){
				return false;
			}
			
			
			
			while ($row = mysql_fetch_array($result_files, MYSQL_BOTH)) {
				if (!$sl_files_sv->move_files("../tmp/".$row["filename"],$destination."/".$id_element."/".$row["filename"])){
					return false;	
				}
			}
			
		
			
			
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."attachments set 
					id_element='".$id_element."',
					state='1'
					WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' and id_field='".$lg."' 
					",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
			return true;
		}else{
			return true;
		}
	}
	
	
	
	
	/**
	* Delete Images
	*/
	public function delete_attachments($id_element,$lg,$type="images"){
	
		$sl_files_dl = new sl_files();
		
		$destination = "../medias/attachments/neti_contents/".$type."/".$lg."/".$this->controller->idr;
		
		if ($sl_files_dl->remove_dir($destination."/".$id_element)) {
			$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."attachments WHERE id_module=".$this->controller->module_id." AND id_element='".$id_element."' and state=1 and id_field='".$lg."'",$this->slash->db_handle) 
									or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			return true;
		}else{
			return false;
		}
				
	}
	
	
	/**
	 * Ajout d'un element
	 */
	private function add_element($type){
		
		if ($this->controller->idr && $this->controller->idr!="root"){

			$c_idr = $this->controller->idr;
				
			$result = mysql_query("SELECT id_rub,position FROM neti_elements WHERE id_rub='".$c_idr."' ORDER BY position DESC LIMIT 1",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			if (mysql_num_rows($result) > 0) {
				$row = mysql_fetch_array($result, MYSQL_ASSOC);
				$position = $row["position"]+1;
			}else{
				$position = 1;
			}
			
			
			mysql_query("INSERT INTO neti_elements 
							(id,id_rub,type,position) value
							('','".$c_idr."','".$type."','".$position."')",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			return mysql_insert_id();
					
		}else{
			return Null;
		}
		
	}
	
	
	
	/**
	* Récupération des valeurs du formulaire elements
	*/
	public function recovery_fields($type) {
		
		switch($type) {
			
			case 1: //Bloc Texte
				$ret = $this->recovery_fields_txt();
				break;
			case 2: //Bloc Image
				$ret = $this->recovery_fields_img();
				break;
			case 3: //Bloc Image + Texte
				$ret = $this->recovery_fields_imgtxt();
				break;
			case 4: //Bloc Fichier
				$ret = $this->recovery_fields_file();
				break;
			case 5: //Bloc Flash
				$ret = $this->recovery_fields_flash();
				break;
			default:
					
		}
		
		return $ret;
			
	}
	
	
	/**
	* Vérification des valeurs du formulaire elements
	* @param $values:Array Object Values
	*/
	public function check_fields($values,$type) {
		
		
		switch($type) {
			//Bloc Texte
			case 1: 
				$ret = $this->check_fields_txt($values);
			break;
			case 2: //Bloc Image
				$ret = $this->check_fields_img($values);
				break;
			case 3: //Bloc Image + Texte
				$ret = $this->check_fields_imgtxt($values);
				break;
			case 4: //Bloc Fichier
				$ret = $this->check_fields_file($values);
				break;
			case 5: //Bloc Flash
				$ret = $this->check_fields_flash($values);
				break;
			default:
					
		}
		
		return $ret;
		
		
	
	}
	
	
	
	
	
	/**
	* Récupération des valeurs du formulaire elements texte
	*/
	public function recovery_fields_txt() {
		
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		
		for($i=0;$i<count($this->controller->lg);$i++){
			$obj["txt-".$this->controller->lg[$i]["id"]] = $this->slash->sl_param($this->controller->module_name."_objtxt-".$this->controller->lg[$i]["id"],"POST");
		}
		
		/*
		echo "<pre>";
		print_r($obj);
		echo "</pre>";
		*/
		
		
		return $obj;
	}
	
	
	/**
	* Vérification des valeurs du formulaire elements texte
	* @param $values:Array Object Values
	*/
	public function check_fields_txt($values) {
	
		$mess = array();
		
		if ($values["txt-".$this->controller->lg[0]["id"]]==""){
			$mess["txt-".$this->controller->lg[0]["id"]]["message"] =  $this->slash->trad_word("NETICONTENTS_ERR-FIELDEMPTY");
		}
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	}
	
	
	/**
	* Récupération des valeurs du formulaire elements image
	*/
	public function recovery_fields_img() {
		
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		$obj["img_align"] = $this->slash->sl_param($this->controller->module_name."_obj_img_align","POST");
		$obj["img_border"] = $this->slash->sl_param($this->controller->module_name."_obj_img_border","POST");
		$obj["img_action"] = $this->slash->sl_param($this->controller->module_name."_obj_img_action","POST");
		$obj["img_url"] = $this->slash->sl_param($this->controller->module_name."_obj_img_url","POST");
		$obj["img_rub"] = $this->slash->sl_param($this->controller->module_name."_obj_img_rub","POST");
		
		$obj["diapo_width"] = $this->slash->sl_param($this->controller->module_name."_obj_diapo_width","POST");
		$obj["diapo_height"] = $this->slash->sl_param($this->controller->module_name."_obj_diapo_height","POST");
		$obj["diapo_time"] = $this->slash->sl_param($this->controller->module_name."_obj_diapo_time","POST");
		$obj["diapo_thumbs"] = $this->slash->sl_param($this->controller->module_name."_obj_diapo_thumbs","POST");
		$obj["diapo_thumbs_position"] = $this->slash->sl_param($this->controller->module_name."_obj_diapo_thumbs_position","POST");
			
		
		return $obj;
	}
	
	
	/**
	* Vérification des valeurs du formulaire elements image
	* @param $values:Array Object Values
	*/
	public function check_fields_img($values) {
	
		$mess = array();
		
		
		if ($values["id"]==0){ // Vérification si au moins 1 images dans la première langue
			
			$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' ORDER BY position",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
					
			if (mysql_num_rows($result_files) == 0) { 
				$mess["img"]["message"] =  $this->slash->trad_word("NETICONTENTS_ERR-NEEDIMAGE");			
			} 
			
		}
		
		/*exit;*/
		
		/**/
		
		
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  "BAD";
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	}
	
	/**
	* Récupération des valeurs du formulaire elements image + texte
	*/
	public function recovery_fields_imgtxt() {
		
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		
		for($i=0;$i<count($this->controller->lg);$i++){
			$obj["imgtxt-".$this->controller->lg[$i]["id"]]["txt"] = $this->slash->sl_param($this->controller->module_name."_objtxt-".$this->controller->lg[$i]["id"],"POST");
			$obj["imgtxt-".$this->controller->lg[$i]["id"]]["img_align"] = $this->slash->sl_param($this->controller->module_name."_obj_img_align-".$this->controller->lg[$i]["id"],"POST");
			$obj["imgtxt-".$this->controller->lg[$i]["id"]]["img_border"] = $this->slash->sl_param($this->controller->module_name."_obj_img_border-".$this->controller->lg[$i]["id"],"POST");
			$obj["imgtxt-".$this->controller->lg[$i]["id"]]["img_action"] = $this->slash->sl_param($this->controller->module_name."_obj_img_action-".$this->controller->lg[$i]["id"],"POST");
			$obj["imgtxt-".$this->controller->lg[$i]["id"]]["img_url"] = $this->slash->sl_param($this->controller->module_name."_obj_img_url-".$this->controller->lg[$i]["id"],"POST");
			$obj["imgtxt-".$this->controller->lg[$i]["id"]]["img_rub"] = $this->slash->sl_param($this->controller->module_name."_obj_img_rub-".$this->controller->lg[$i]["id"],"POST");

		}
		
		/*
		echo "<pre>";
		echo "ID : ".$obj["id"]."<br/>";
			print_r($obj);
		echo "</pre>";
		*/
		
		
		
		
		return $obj;
	}
	
	
	/**
	* Vérification des valeurs du formulaire elements image + texte
	* @param $values:Array Object Values
	*/
	public function check_fields_imgtxt($values) {
	
		$mess = array();
		
		/*
		if ($values["id"]==0){ // Vérification si au moins 1 images dans la première langue
			
			$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' ORDER BY position",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
					
			if (mysql_num_rows($result_files) == 0) { 
				$mess["img"]["message"] =  $this->slash->trad_word("NETICONTENTS_ERR-NEEDIMAGE");			
			} 
			
		}*/
		
		/*exit;*/
		
		/**/
		
		
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  "BAD";
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	}
	
	
	
	
	
	/**
	* Récupération des valeurs du formulaire elements fichier
	*/
	public function recovery_fields_file() {
		
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		for($i=0;$i<count($this->controller->lg);$i++){
			$obj["file-".$this->controller->lg[$i]["id"]]["title"] = $this->slash->sl_param($this->controller->module_name."_objtitle-".$this->controller->lg[$i]["id"],"POST");
		}
			
		return $obj;
	}
	
	
	/**
	* Vérification des valeurs du formulaire elements fichier
	* @param $values:Array Object Values
	*/
	public function check_fields_file($values) {
	
		$mess = array();
		
		/*
		if ($values["id"]==0){ // Vérification si au moins 1 images dans la première langue
			
			$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' ORDER BY position",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
					
			if (mysql_num_rows($result_files) == 0) { 
				$mess["img"]["message"] =  $this->slash->trad_word("NETICONTENTS_ERR-NEEDIMAGE");			
			} 
			
		}*/
		
		/*exit;*/
		
		/**/
		
		
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  "BAD";
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	}
	
	
	
	
	/**
	* Récupération des valeurs du formulaire elements Flash (5)
	*/
	public function recovery_fields_flash() {
		
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		for($i=0;$i<count($this->controller->lg);$i++){
			$obj["flash-".$this->controller->lg[$i]["id"]]["title"] = $this->slash->sl_param($this->controller->module_name."_objtitle-".$this->controller->lg[$i]["id"],"POST");
			$obj["flash-".$this->controller->lg[$i]["id"]]["width"] = $this->slash->sl_param($this->controller->module_name."_objwidth-".$this->controller->lg[$i]["id"],"POST");
			$obj["flash-".$this->controller->lg[$i]["id"]]["height"] = $this->slash->sl_param($this->controller->module_name."_objheight-".$this->controller->lg[$i]["id"],"POST");
		}
			
		return $obj;
	}
	
	
	/**
	* Vérification des valeurs du formulaire elements Flash (5)
	* @param $values:Array Object Values
	*/
	public function check_fields_flash($values) {
	
		$mess = array();
		
		/*
		if ($values["id"]==0){ // Vérification si au moins 1 images dans la première langue
			
			$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' ORDER BY position",$this->slash->db_handle) 
				or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
					
			if (mysql_num_rows($result_files) == 0) { 
				$mess["img"]["message"] =  $this->slash->trad_word("NETICONTENTS_ERR-NEEDIMAGE");			
			} 
			
		}*/
		
		/*exit;*/
		
		/**/
		
		
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  "BAD";
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	}
	
	
}

?>