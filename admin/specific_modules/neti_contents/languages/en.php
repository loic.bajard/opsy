<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin netissima module languages
* @version		en.php - Version 11.2.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

//Module NETI_CONTENTS
define("NETICONTENTS_TITLE", "Contents config");
define("NETICONTENTS_RUBTITLE", " : : Rubrique de votre site");

define("NETICONTENTS_DELETE_CONFIRM", "Delete this article ?");
?>
