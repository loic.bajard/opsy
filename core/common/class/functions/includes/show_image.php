<?php
/**
 * Resize image
 */
function resize_image ($source,$width="auto",$height="auto",$format="auto",$quality=100) {
		
		
		
		$i_size = getimagesize($source);
		 
		switch ($i_size[2]) { 
			
			case 1: //gif
				header('Content-type: ' .image_type_to_mime_type(IMAGETYPE_GIF));
				$img_src=imagecreatefromgif($source);
			break;
			
			case 2: // jpg and jpeg
				header('Content-type: ' .image_type_to_mime_type(IMAGETYPE_JPEG));
				$img_src=imagecreatefromjpeg($source);
			break;
			
			case 3: // png
				header('Content-type: ' .image_type_to_mime_type(IMAGETYPE_PNG));
				$img_src=imagecreatefrompng($source);
			break;
			
			default:
				return 0;
		}
	
		if ($width == "auto" && $height== "auto") {
			$img_dest=imagecreatetruecolor($i_size[0],$i_size[1]); 
			ImageCopyResampled($img_dest, $img_src, 0, 0, 0, 0,$i_size[0], $i_size[1], $i_size[0], $i_size[1]);
		}
		
		if ($width != "auto" || $height != "auto" ) {
			
			
			if ($i_size[0]>$width || $i_size[1]>$height) {
				  $ratio_l=$width/$i_size[0];
				  $ratio_h=$height/$i_size[1];
				  if ($ratio_h > $ratio_l) { $ratio = $ratio_l; } else { $ratio = $ratio_h; }
			} else {
				$ratio = 1;
			}
			

			//$img_dest=imagecreatetruecolor($width,$height);	
			$img_dest=imagecreatetruecolor(round($i_size[0]*$ratio),round($i_size[1]*$ratio));
			ImageCopyResampled($img_dest, $img_src, 0, 0, 0, 0,round($i_size[0]*$ratio), round($i_size[1]*$ratio), $i_size[0], $i_size[1]);
		}
		
		if ($format=="auto"){
			$format=$i_size[2];
		}
		
				
		switch ($format) { 
			
			case 1: //gif
				if (!imagegif($img_dest)) {
					return 0;
				}else{
					imagedestroy($img_dest);
				}
			break;
			
			case 2: // jpg and jpeg
				if (!imagejpeg($img_dest,null,$quality)) {
					return 0;
				}else{
					imagedestroy($img_dest);
				}
			break;
			
			case 3: // png
				if ($quality == 100) {
						if (!imagepng($img_dest,null,0)) {
							return 0;
						}else{
							imagedestroy($img_dest);
						}
				}else{
						if (!imagepng($img_dest,null,$quality)) {
							return 0;
						}else{
							imagedestroy($img_dest);
						}
				}
			break;
			
			default:
				return 0;
		}
			
	
	return 1;
}


// --- MAIN --- //
$url;
$width;
$height;

if (isset($_GET["url"]) && $_GET["url"] != ""){$url = $_GET["url"];}
if (isset($_GET["width"]) && $_GET["width"] != ""){$width = $_GET["width"];} else { $width="auto"; }
if (isset($_GET["height"]) && $_GET["height"] != ""){$height = $_GET["height"];} else { $height="auto"; }

if (file_exists($url)) {
	resize_image($url,$width,$height);
}
?>