<?php
/**
* @package		SLASH-CMS
* @subpackage	CONTROLLER ABSTRACT CLASS
* @internal     Module Controller
* @version		sla_controller.php - Version 12.3.02
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

abstract class slaController{

	public $slash; //Core Reference
	public $params;
	public $module_id;
	
	protected $mode;
	protected $message;
	protected $errors;
	protected $datas;

	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {
       $this->slash = $core_class_ref;
       $this->module_id = $module_id;
	   
	   $this->sla_construct();
	}
	
	public function sla_construct() {}
	
	public function initialise(){} //Initialise function 
	public function load_header(){} //Load header function
	public function load_footer(){} //Load footer function
	public function load(){} //Load module function
	public function execute(){} //Execute function
	
}

?>