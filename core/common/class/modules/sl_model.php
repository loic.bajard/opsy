<?php
/**
* @package		SLASH-CMS
* @subpackage	MODEL ABSTRACT CLASS
* @internal     Module Model
* @version		sl_model.php - Version 12.3.02
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

abstract class slModel{


	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
		
		$this->sl_construct();
	}
	
	public function sl_construct() {
	
	}
	
}

?>