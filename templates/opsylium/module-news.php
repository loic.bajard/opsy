<div class="module-news">
	<h2>Actualités</h2>
	<div class="carousel">
		<?php

			$stmt = $dbh->prepare('SELECT sl_news.id, sl_news.title, sl_attachments.filename FROM sl_news INNER JOIN sl_attachments ON sl_news.id = sl_attachments.id_element WHERE sl_attachments.id_module = 27 AND sl_news.enabled = 1');
			$stmt->execute();
			$news = $stmt->fetchAll(PDO::FETCH_OBJ);

			for ($i = 0; $i < count($news); $i++) {

				$photos = json_decode($news[$i]->photos);

				echo '<a href="index.php?news=on">';
					echo '<div class="item" style="background-image:url(medias/attachments/sl_news/'. $news[$i]->id .'/'. $news[$i]->filename .')">';
							echo '<div class="caption">';
								echo '<h2>'. truncate(utf8_decode($news[$i]->title), 50) .'</h2>';
							echo '</div>';
					echo '</div>';
				echo '</a>';

			}

		?>
	</div>
</div>