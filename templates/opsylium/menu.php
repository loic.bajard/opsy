<?php

	echo '<ul>';

		echo '<li><a href="index.php"><i class="fa fa-home"></i></a></li>';
		
		// PROGRAMMES NEUFS

		echo '<li>';
			echo '<a href="index.php?list=tout&filter=neuf">Programmes neufs</a>';
			echo '<ul>';

				echo '<li>';
					echo '<a href="index.php?list=region&name=bourgogne&filter=neuf">Bourgogne</a>';
					echo '<ul>';

						// TOUS LES 21

						$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 21 AND type = "neuf" GROUP BY codepostal');
						$stmt->execute();
						$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

						for ($i = 0; $i < count($villes); $i++) {

							$ville = ucwords(strtolower($villes[$i]->ville));
							$cp = $villes[$i]->codepostal;

							echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=neuf">'. $ville .'</a></li>';

						}

					echo '</ul>';
				echo '</li>';

				echo '<li>';
					echo '<a href="index.php?list=region&name=rhone-alpes&filter=neuf">Rhône-Alpes</a>';
					echo '<ul>';
						echo '<li>';

						// TOUS LES 69

						$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 69 AND type = "neuf" GROUP BY codepostal');
						$stmt->execute();
						$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

						for ($i = 0; $i < count($villes); $i++) {

							$ville = ucwords(strtolower($villes[$i]->ville));
							$cp = $villes[$i]->codepostal;

							echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=neuf">'. $ville .'</a></li>';

						}

						echo '</li>';
						echo '<li>';

							// TOUS LES 74

							$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 74 AND type = "neuf" GROUP BY codepostal');
							$stmt->execute();
							$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

							for ($i = 0; $i < count($villes); $i++) {

								$ville = ucwords(strtolower($villes[$i]->ville));
								$cp = $villes[$i]->codepostal;

								echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=neuf">'. $ville .'</a></li>';

							}

						echo '</li>';
					echo '</ul>';
				echo '</li>';

			echo '</ul>';
		echo '</li>';

		// DEFISCALISATION

		echo '<li>';
			echo '<a href="index.php?list=tout&filter=defisc">Défiscalisation</a>';
			echo '<ul>';

				echo '<li>';
					echo '<a href="index.php?list=region&name=bourgogne&filter=defisc">Bourgogne</a>';
					echo '<ul>';
						echo '<li>';

						// TOUS LES 21

						$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 21 AND dispositif_fiscal != "" GROUP BY codepostal');
						$stmt->execute();
						$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

						for ($i = 0; $i < count($villes); $i++) {

							$ville = ucwords(strtolower($villes[$i]->ville));
							$cp = $villes[$i]->codepostal;

							echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=defisc">'. $ville .'</a></li>';

						}

						echo '</li>';
					echo '</ul>';
				echo '</li>';

				echo '<li>';
					echo '<a href="index.php?list=region&name=rhone-alpes&filter=defisc">Rhône-Alpes</a>';
					echo '<ul>';
						echo '<li>';

						// TOUS LES 69

						$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 69 AND dispositif_fiscal != "" GROUP BY codepostal');
						$stmt->execute();
						$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

						for ($i = 0; $i < count($villes); $i++) {

							$ville = ucwords(strtolower($villes[$i]->ville));
							$cp = $villes[$i]->codepostal;

							echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=defisc">'. $ville .'</a></li>';

						}

						echo '</li>';
						echo '<li>';

						// TOUS LES 74

						$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 74 AND dispositif_fiscal != "" GROUP BY codepostal');
						$stmt->execute();
						$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

						for ($i = 0; $i < count($villes); $i++) {

							$ville = ucwords(strtolower($villes[$i]->ville));
							$cp = $villes[$i]->codepostal;

							echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=defisc">'. $ville .'</a></li>';

						}

						echo '</li>';
					echo '</ul>';
				echo '</li>';

			echo '</ul>';
		echo '</li>';

		// PROGRAMMES ANCIENS

		echo '<li>';
			echo '<a href="index.php?list=tout&filter=ancien">Anciens</a>';
			echo '<ul>';

				echo '<li>';
					echo '<a href="index.php?list=region&name=bourgogne&filter=ancien">Bourgogne</a>';
					echo '<ul>';
						echo '<li>';

						// TOUS LES 21

						$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 21 AND type = "ancien" GROUP BY codepostal');
						$stmt->execute();
						$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

						for ($i = 0; $i < count($villes); $i++) {

							$ville = ucwords(strtolower($villes[$i]->ville));
							$cp = $villes[$i]->codepostal;

							echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=ancien">'. $ville .'</a></li>';

						}

						echo '</li>';
					echo '</ul>';
				echo '</li>';

				echo '<li>';
					echo '<a href="index.php?list=region&name=rhone-alpes&filter=ancien">Rhône-Alpes</a>';
					echo '<ul>';
						echo '<li>';

						// TOUS LES 69

						$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 69 AND type = "ancien" GROUP BY codepostal');
						$stmt->execute();
						$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

						for ($i = 0; $i < count($villes); $i++) {

							$ville = ucwords(strtolower($villes[$i]->ville));
							$cp = $villes[$i]->codepostal;

							echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=ancien">'. $ville .'</a></li>';

						}

						echo '</li>';
						echo '<li>';

							// TOUS LES 74

							$stmt = $dbh->prepare('SELECT ville, codepostal, LEFT(codepostal, 2) AS cp FROM biens WHERE LEFT(codepostal, 2) = 74 AND type = "ancien" GROUP BY codepostal');
							$stmt->execute();
							$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

							for ($i = 0; $i < count($villes); $i++) {

								$ville = ucwords(strtolower($villes[$i]->ville));
								$cp = $villes[$i]->codepostal;

								echo '<li><a href="index.php?list=ville&cp='. $cp .'&filter=ancien">'. $ville .'</a></li>';

							}

						echo '</li>';
					echo '</ul>';
				echo '</li>';

			echo '</ul>';
		echo '</li>';

		echo '<li><a href="actualites">Actualités</a></li>';

		echo '<li>';
			echo '<a href="#">Qui sommes nous ?</a>';
			echo '<ul>';
				echo '<li><a href="l-agence-34.php">L\'agence</a></li>';
				echo '<li><a href="#" class="recall">Être rappelé</a></li>';
			echo '</ul>';
		echo '</li>';

		echo '<li><a href="contact">Contact</a></li>';

	echo '</ul>';

?>