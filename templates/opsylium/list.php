<?php

	if ($this->sl_param('list') === 'tout') {

		$sql = 'SELECT id, mandat, ville, photos, descriptif, dispositif_fiscal, livraison FROM biens WHERE id != 0';

	} elseif ($this->sl_param('list') === 'region') {

		// REGIONS

		switch ($this->sl_param('name')) {

			case 'bourgogne':
				$departements = '21, 89, 71, 58';
				break;
			case 'rhone-alpes':
				$departements = '69, 42, 01, 74, 73, 38, 26, 07';
				break;
		}

		$sql = 'SELECT id, mandat, ville, photos, descriptif, dispositif_fiscal, livraison FROM biens WHERE LEFT(codepostal, 2) IN ('. $departements .')';

	} elseif ($this->sl_param('list') === 'zone') {

		// ZONES

		switch ($this->sl_param('name')) {

			case 'grand-dijon':
				$cp = '21';
				break;
			case 'grand-lyon':
				$cp = '69';
				break;
			case 'pays-annecy':
				$cp = '74';
				break;
		}

		$sql = 'SELECT id, mandat, ville, photos, descriptif, dispositif_fiscal, livraison FROM biens WHERE LEFT(codepostal, 2) = '. $cp;

	} elseif ($this->sl_param('list') === 'ville') { 

		// VILLE

		$sql = 'SELECT id, mandat, ville, photos, descriptif, dispositif_fiscal, livraison FROM biens WHERE codepostal = '. $this->sl_param('cp');

	}

	// FILTER

	if ($this->sl_param('filter') === 'neuf') {

		$sql .= ' AND type = "neuf"';
		echo '<h1>Programmes neufs</h1>';

	}  elseif ($this->sl_param('filter') === 'ancien') {

		$sql .= ' AND type = "ancien"';
		echo '<h1>Biens anciens</h1>';

	} elseif ($this->sl_param('filter') === 'defisc') {

		$sql .= ' AND dispositif_fiscal != ""';
		echo '<h1>Défiscalisation</h1>';

	}

?>

<div class="list">
	<?php

		$stmt = $dbh->prepare($sql);
		$stmt->execute();
		$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

		for ($i = 0; $i < count($villes); $i++) {

			$photos = json_decode($villes[$i]->photos);

			echo '<div class="item">';
				echo '<a class="preview" style="background-image:url('. $photos[0] .')" href="bien/'. $villes[$i]->id .'">';
					if (strtolower($villes[$i]->dispositif_fiscal) === 'pinel') {
						echo '<span class="pastilles pinel"><img src="templates/opsylium/images/pinel.png" alt="Loi Pinel"></span>';
					}
					if (strtolower($villes[$i]->livraison) !== '') {
						echo '<span class="pastilles building">'. $villes[$i]->livraison .'</span>';
					}
				echo '</a>';
				echo '<div class="desc">';
					echo '<a href="bien/'. $villes[$i]->id .'">';
						echo '<h2>'. $villes[$i]->ville .' - '. $villes[$i]->mandat .'</h2>';
					echo '</a>';
					echo '<p>'. truncate($villes[$i]->descriptif, 300) .'</p>';
				echo '</div>';
				echo '<div class="clearfix"></div>';
			echo '</div>';

		}

		if (count($villes) === 0) { echo '<p>Aucun résultat</p>'; }

	?>
</div>