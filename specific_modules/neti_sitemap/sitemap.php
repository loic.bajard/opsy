<?php 

include ("../../core/config/configuration.php");

include ("../../core/common/class/functions/includes/sl_text.php");

class sl_sitemap{
	
	private $params;
	
	private $links;
	
	private $db_selected;
	
	private $domainpath;
	
	private $submenu;
	
	/**
	* Constructor
	*/
	function __construct(){
		
		$this->params = new SLConfig;
		
		$this->links = new sl_text;
		
		$db_handle = mysql_connect($this->params->mysql_host, $this->params->mysql_user, $this->params->mysql_password);
		
		$this->db_selected = mysql_select_db($this->params->mysql_database, $db_handle);
		
		$this->submenu = array();
		
	}
	
	/**
	* Private Load functions
	*/
	/** Load domainpath **/
	private function load_domainpath(){
		$requete_domainpath = mysql_query("
								SELECT 
									config_value
								FROM
									sl_config 
								WHERE 
									config_name = 'site_url'" ) or die ( mysql_error() );
		$tab_domainpath = mysql_fetch_array($requete_domainpath);
		
		$this->domainpath = $tab_domainpath["config_value"];
	}
	
	/** Load unauthorized url **/
	private function load_submenu(){
		$requete_submenu = mysql_query("
								SELECT DISTINCT
									id_top 
								FROM
									neti_rubpos
								WHERE 
									id_top != '0' 
								ORDER BY 
									id_top ASC" ) or die ( mysql_error() );
		
		$i = 0;
		while($tab_submenu = mysql_fetch_array($requete_submenu)){
			$this->submenu[$i] = $tab_submenu["id_top"];
			$i++;
		}
	}
	
	/** Display sitemap url **/
	private function display_url(){
		$requete_url = mysql_query("
								SELECT 
									rp.id,
									rt.title 
								FROM
									neti_rubpos AS rp, 
									neti_rubtitles AS rt 
								WHERE 
									rt.id_rub = rp.id AND 
									rp.enabled = '1' AND 
									rp.hidden = '0' 
								ORDER BY 
									position ASC" ) or die ( mysql_error() );
		
		
		while($tab = mysql_fetch_array($requete_url)){
			
			if(!in_array($tab["id"],$this->submenu)){
				echo "<url><loc>".$this->domainpath.$this->links->text2url($tab["title"])."-".$tab["id"].".php</loc></url>"."\n";
			}
			
		}
	}
	
	/**
	* Init display XML file
	*/
	public function init_sitemap(){
	
		if ($this->db_selected) {
			
			echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
			echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
			
			$this->load_domainpath();
			
			$this->load_submenu();
			
			$this->display_url();
			
			echo '</urlset>'."\n";
		}
	}
}

$sitemap = new sl_sitemap;

$sitemap->init_sitemap();

?>