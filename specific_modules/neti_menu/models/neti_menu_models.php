<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin netissima functions
* @version		netissima.php - Version 11.2.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_menu{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	/**
	* Loading neti_menu function
	* @param $parent parent id
	*/
	public function load_neti_menu($parent) {


		if ($this->slash->sl_param("mobile") == 1 || $this->slash->mobile->isMobile() == true) {

			//si le parent est "Société"
			if ($this->slash->sl_param("parent") == 34) {

				//on cherche les sous rubriques de contact + "informations"
				$result=mysql_query("SELECT * FROM neti_rubpos
									WHERE id_top='34' OR id = '45' AND enabled=1 AND hidden=0 
									ORDER BY position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
			else{

				//sinon on cherches les catégories de résidences"
				$result=mysql_query("SELECT * FROM neti_rubpos
									WHERE id_top='44' AND enabled=1 AND hidden=0 
									ORDER BY position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());

			}

			if( mysql_num_rows($result)  > 0 ) {
				
				while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
					$this->controller->view->start_neti_menu($row);
				}
			}

			
		}

		else{
		
			$result=mysql_query("SELECT * FROM neti_rubpos
										WHERE id_top='".$parent."' AND enabled=1 AND hidden=0 
										ORDER BY position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			if( mysql_num_rows($result)  > 0 ) {
				
				if ($parent != 0 ) { $this->controller->view->start_under_neti_menu();}
				
				while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
					$this->controller->view->start_neti_menu($row);
					$this->load_neti_menu($row["id"]);
				}
				
				if ($parent != 0 ) { $this->controller->view->end_under_neti_menu();}
				
			} else {
				$this->controller->view->end_neti_menu();
			}

		}
		
	}
	
	

	public function load_neti_mobile_ssmenu($parent) {
		
		$result=mysql_query("SELECT * FROM neti_rubpos
									WHERE id_top='".$parent."' AND enabled=1 AND hidden=0 
									ORDER BY position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if( mysql_num_rows($result)  > 0 ) {
		
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$this->controller->view->start_neti_mobile_ssmenu($row);
			}
		}
		
	}


	
	/**
	 * Récupération du titre d'une rubrique
	 * @param $id rub ID
	 * @param $lg 
	 */
	public function get_title ($id,$lg){
		$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_rub='".$id."' AND id_lg='".$lg."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row["title"];
		}else{
			return Null;
		}
		
	}
	
	public function check_if_child($id){
		$result = mysql_query("SELECT * FROM neti_rubpos WHERE id_top='".$id."' AND enabled='1'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			return true;
		}else{
			return false;
		}
	}
	
	public function get_parent_id($id_child){
		$result = mysql_query("SELECT id_top FROM neti_rubpos WHERE id='".$id_child."' AND enabled='1'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			
			return $row["id_top"];
		}else{
			return 0;
		}
	}
	
}