<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_contents_view
* @internal     Admin netissima module
* @version		neti_contents_view.php - Version 11.2.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_view {

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}

	/**
	 * Show global HTML Header
	 */
	public function header () {
	
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/interface/js/interface.js'></script> \n";
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/lightbox/js/lightbox.js'></script> \n";
/*		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/preload/js/preloadCssImages.js'></script> \n";
*/		
		

		//Fonctions JavaScript NETIssima
		$this->controller->neti_core_view->fct_js();
		
		//Styles NETIssima
		echo "<link rel='stylesheet' type='text/css' media='screen' href='specific_modules/neti_contents/views/mrpromotion/css/styles.css' /> \n"; 
		echo "<link rel='stylesheet' type='text/css' media='screen' href='specific_modules/neti_contents/views/mrpromotion/css/assets.css' /> \n"; 
		
		echo "<link rel='stylesheet' type='text/css' href='core/plugins/jquery_plugins/lightbox/css/lightbox.css' /> \n";
	} 
	  
	public function l_header () {
		
		//Jquery inc.
		
		echo "<script type='text/javascript' src='core/plugins/jquery/jquery.timers.js'></script> \n";
		echo "<script type='text/javascript' src='core/plugins/jquery/jquery.easing.js'></script> \n";
		echo "<script type='text/javascript' src='core/plugins/jquery/jquery.cookie.js'></script> \n";
		
		//Galleryview
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/galleryview/js/galleryview.js'></script> \n";
		echo "<link rel='stylesheet' type='text/css' media='screen' href='core/plugins/jquery_plugins/galleryview/css/galleryview.css' /> \n";
		
		//Simple tree
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/simpletree/js/persistence.simpletree.js'></script> \n";
		echo "<link rel='stylesheet' type='text/css' media='screen' href='core/plugins/jquery_plugins/simpletree/css/simpletree.css' /> \n";
		
		
		
	}
	
	public function f_header () {
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/ajaxupload/js/ajaxupload.js'></script> \n";
		echo "<link rel='stylesheet' type='text/css' href='core/plugins/jquery_plugins/tabs/css/tabs.css' media='screen'>";
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/ui/js/ui.core.js'></script> \n";
				
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/tabs/js/tabs.js'></script> \n";
		echo "<script type='text/javascript' src='core/plugins/tiny_mce/jquery.tinymce.js'></script> \n";
		echo "<script type='text/javascript' src='core/plugins/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php'></script> \n";
		
	}
	
	/**
	 * Affiche la page principale de la gestion de contenu NETIssima
	 * @param $message message
	 */
	public function show_main($message="") {
							
		for ($i=0;$i<count($this->controller->lg);$i++) {
			if ($this->controller->active_lg==$this->controller->lg[$i]["id"]) { 
				$this->controller->neti_core->make_preview($this->controller->idr,$this->controller->lg[$i]["id"]);
			}
		}

	}


	
	
	/**
	 * HTML footer
	 */
	public function footer() {
		/*echo "<script type='text/javascript'>
			$(document).ready(function(){ 
				$('.lightbox').lightbox();
				$('#message').css('opacity',1);
				$('#message').stopAll().pause(5000).fadeTo(400,0, function() { $(this).hide(); } );
				$.preloadCssImages();	
				
		}); 			
		</script>";*/
	}
	
	/**
	 * HTML footer
	 */
	public function l_footer() {
		

	}
	
	/**
	 * HTML footer
	 */
	public function f_footer() {
		
	}
	

	
	
	
}





?>