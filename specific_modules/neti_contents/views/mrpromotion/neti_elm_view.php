<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_elm_view
* @internal     Admin netissima module
* @version		neti_elm_view.php - Version 11.3.17
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

class neti_elm_view {


	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	
	/**
	 * Affiche le titre de la page
	 * @title Titre
	 */
	public function show_title($title){
		echo "<h1>".$title."</h1>";
	}
	
	/**
	 * Affiche un paragraphe de type texte (1)
	 * @obj Objet texte
	 */
	public function show_txt($obj){
		if ($obj["txt"]==""){
			echo "<div class='neti-page-text'>&nbsp;</div>";
		}else{
			echo "<div class='neti-page-text'>".$obj["txt"]."</div>";
		}
	}
	
	
	/**
	 * Affiche un paragraphe de type image (2)
	 * @obj Objet image
	 */
	public function show_img($obj){
		
		if (count($obj["images"]) == 1){ //Affiche 1 seul image
			$url = "medias/attachments/neti_contents/images/".$obj["images"][0]["id_field"]."/".$this->controller->idr."/".$obj["images"][0]["id_element"]."/".$obj["images"][0]["filename"];
			
			if ($obj["img_border"] == 2){
				$class = "neti-img-border";
			}elseif($obj["img_border"] == 3){
				$class = "neti-img-shadow";
			}else{
				$class = "";
			}
			
			if ($obj["img_align"] == 2){
				echo "<div align='center'>";
			}elseif($obj["img_align"] == 3){
				echo "<div align='right'>";
			}else{
				echo "<div align='left'>";
			}
			
				echo "<div class='".$class."'>";
				if ($obj["img_action"] == 2) { echo "<a href='".$url."' class='lightbox' title=''>"; }
				echo sl_images::show_image($url,905);
				if ($obj["img_action"] == 2) { echo "</a>"; }
				echo "</div>";
				
			echo "</div>";
		
		}elseif(count($obj["images"]) > 1) { //Diaporama
			
			echo "<ul id='neti-gallery".$obj["id"]."'>";
			
			for ($i=0;$i<count($obj["images"]);$i++){
				$url = "medias/attachments/neti_contents/images/".$obj["images"][$i]["id_field"]."/".$this->controller->idr."/".$obj["images"][$i]["id_element"]."/".$obj["images"][$i]["filename"];
				echo "<li><img src='".$url."' /></li>";
			}
			
			echo "</ul>";
			
			
			echo "
			<script type='text/javascript'> 
			
				$(document).ready(function(){
					$('#neti-gallery".$obj["id"]."').galleryView({
						show_filmstrip: false,	 
						panel_width: 600,
	        			panel_height: 400,
	        			panel_scale: 'nocrop',
	        			panel_animation: 'none'				
					});
					
				});
				
			</script>";
			
		}else{ //Pas d'image.
			echo "Aucune image de charg&eacute;";
		}
		
		
		
	}
	
	/**
	 * Affiche un paragraphe de type image + texte (3)
	 * @obj Objet image + texte
	 */
	public function show_imgtxt($obj){
		
		if (count($obj["images"]) > 0){ //Affiche 1 seul image
			
			$url = "medias/attachments/neti_contents/images/".$obj["images"][0]["id_field"]."/".$this->controller->idr."/".$obj["images"][0]["id_element"]."/".$obj["images"][0]["filename"];
			
			if ($obj["img_border"] == 2){
					$class = "neti-img-border";
				}elseif($obj["img_border"] == 3){
					$class = "neti-img-shadow";
				}else{
					$class = "";
				}
			
			
			if ($obj["img_align"] == 0 || $obj["img_align"] == 1) {

				echo "<div class='table'>";
				
				echo "<div class='neti-page-img ".$class."'>";
				if ($obj["img_action"] == 2) { echo "<a href='".$url."' class='lightbox' title=''>"; }
				echo sl_images::show_image($url);
				if ($obj["img_action"] == 2) { echo "</a>"; }
				echo "</div>";
				
				if ($obj["txt"]==""){
					echo "<div class='neti-page-text'>&nbsp;</div>";
				}else{
					echo "<div class='neti-page-text'>".$obj["txt"]."</div>";
				}
				echo "</div>";


			}else{


				echo "<div class='table'>";
				if ($obj["txt"]==""){
					echo "<div class='neti-page-text'>&nbsp;</div>";
				}else{
					echo "<div class='neti-page-text'>".$obj["txt"]."</div>";
				}
				
				echo "<div class='neti-page-img ".$class."'>";
				if ($obj["img_action"] == 2) { echo "<a href='".$url."' class='lightbox' title=''>"; }
				echo sl_images::show_image($url);
				if ($obj["img_action"] == 2) { echo "</a>"; }
				echo "</div>";
				
				
				echo "</div>";
			}

		}else{ //Pas d'image.
			echo "Aucune image de charg&eacute;";
		}
		
		
	}
	
	/**
	 * Affiche un paragraphe de type fichier (4)
	 * @obj Objet fichier
	 */
	public function show_file($obj){
		if (count($obj["files"]) == 1){ //Affiche 1 seul image
			
			$url = "medias/attachments/neti_contents/files/".$obj["files"][0]["id_field"]."/".$this->controller->idr."/".$obj["files"][0]["id_element"]."/".$obj["files"][0]["filename"];
			
			
			$filestools = new sl_files();
			$ext_file = strtolower($filestools->get_file_extension($obj["files"][0]["filename"]));
			
			
			echo "<div class='telecharger'>";	
				echo "<div class='sous-telecharger'>";	
			if (file_exists("admin/templates/system/images/extensions/32x32/".substr($ext_file, 1).".png")) {
					//echo "<img src='admin/templates/system/images/extensions/32x32/".substr($ext_file, 1).".png' align='absmiddle'/>&nbsp;";
					echo "<a href='".$url."' target='_blank'><img src='admin/templates/system/images/extensions/32x32/".substr($ext_file, 1).".png' align='absmiddle'/>&nbsp;".$obj["title"]."</a>";
			}else{
					echo "<a href='".$url."' target='_blank'><img src='admin/templates/system/images/extensions/32x32/none.png' align='absmiddle'/>&nbsp;".$obj["title"]."</a>";
					
			}
				
			}
				echo "</div>";
			echo "</div>";
		
		
	}
	
	
	/**
	 * Affiche un paragraphe de type flash (5)
	 * @obj Objet flash
	 */
	public function show_flash($obj){
		echo "Anim flash";
	}
	
	/**
	 * Affiche un paragraphe de type saut de ligne (6)
	 */
	public function show_br(){
		echo "<br/>";
	}
	
	
	
	
}

?>