 <?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_contents
* @internal     Netissima module
* @version		neti_contents.php - Version 11.2.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/
 
 // Include Configuration
Include ("admin/specific_modules/neti_contents/config/neti_config.php");
 
// Include Default Views
include ("views/mrpromotion/neti_view.php");
include ("views/mrpromotion/neti_core_view.php");
include ("views/mrpromotion/neti_rub_view.php");
include ("views/mrpromotion/neti_elm_view.php");

// Include models
include ("models/neti_core.php");
include ("models/neti_rub.php");
include ("models/neti_elm.php");

class neti_contents_controller{

	public $slash; //Core Reference
	public $params;
	public $module_name = "neti_contents";
	public $module_id;
	
	public $mode;
	public $message;
	public $errors;
	public $datas;
	public $lg;
	public $idr;
	
	public $active_lg;
	
	//Config
	public $neti_config;
	
	// Models
	public $neti_core;
	public $neti_rub;
	public $neti_elm;
	
	// Views
	public $neti_view;
	public $neti_core_view;
	public $neti_rub_view;
	public $neti_elm_view;

	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {
		
		
		$this->slash = $core_class_ref;
		$this->module_id = $module_id;
		
		$this->lg = $this->slash->get_active_lang();

		//Config
		$this->neti_config = new neti_config($this); // On déclare l'objet pour l'utilisation des fct de paragraphes
		
		//Models
		$this->neti_core = new neti_core($this); // On déclare l'objet pour l'utilisation des fct propres à NETIssima
		$this->neti_rub = new neti_rub($this); // On déclare l'objet pour l'utilisation des fct de rubriquage
		$this->neti_elm = new neti_elm($this); // On déclare l'objet pour l'utilisation des fct de paragraphes
		
		//Views
		$this->neti_view = new neti_view($this); // Vue Principale
		$this->neti_core_view = new neti_core_view($this); // Vue Netissima
		$this->neti_rub_view = new neti_rub_view($this); // Vue Rubriques
		$this->neti_elm_view = new neti_elm_view($this); // Vue Elements
		
		$this->load_params(); //Load params
	}

	/**
	* Initialise function 
	* Require function by slash core
	*/
	public function initialise() {
		//no global initialisation for this module
	}
	
	/**
	 * Load header function
	 */
	public function load_header(){
		$this->neti_view->header(); //Global header
		
		switch ($this->mode) {
			case "show":
				$this->neti_view->l_header(); //List header
			break;
			
			default:
				//nothing
		}
	}
	
	/**
	 * Load footer function
	 */
	public function load_footer(){
		$this->neti_view->footer(); //Global footer
		
		switch ($this->mode) {
			case "show":
				$this->neti_view->l_footer(); //List footer
			break;
			
			break;
			default:
				//nothing
		}
	}
	
	/**
	* Load module function
	* Require function by slash core
	*/
	public function load() {


		switch ($this->mode) {
			

			case "show": //Page principale			
				$this->neti_view->show_main($this->message);
				break;
			default: //Page principale
				$this->neti_view->show_main($this->message);
		}
	}
	
	
	/**
	* Execute function
	* Require function by slash core
	*/
	public function execute() {
		// no constant execute
	}
	
	
	/**
	 * Charge les paramètres
	 */
	private function load_params() {
		
		//Enregistrement rubrique en cours
		
		if ($this->slash->sl_param("idr")) { $_SESSION["front_neti_idr"] = $this->slash->sl_param("idr"); }
		else {$_SESSION["front_neti_idr"] = 57;}
		if (!isset($_SESSION["front_neti_idr"])) { $_SESSION["front_neti_idr"] = "root"; }
		$this->idr = $_SESSION["front_neti_idr"];
		
		$url_language = "";
		
		//Enregistrement langue en cours
		if ($this->slash->sl_param("lg")) { $_SESSION["front_neti_lg"] = $this->slash->sl_param("lg"); }
		if (!isset($_SESSION["front_neti_lg"])) { $_SESSION["front_neti_lg"] = $this->lg[0]['id']; }
		$this->active_lg = $_SESSION["front_neti_lg"];
		
		/*if (isset($_SESSION["front_user_language"])) {
			$url_language = $_SESSION["front_user_language"];
		}
		
		if($url_language == 'en'){
			$this->active_lg = 2;
		}
		else{
			$this->active_lg = 1;
		}*/
		
		switch ($this->slash->sl_param($this->module_name."_act")) {
			
			
			case 1:
			
			break;
			default: // default view
				$this->mode = "show";
		}
	
	}
	


}



?>
