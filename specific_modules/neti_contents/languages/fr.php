<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin netissima module languages
* @version		fr.php - Version 11.2.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

// Titres
define("NETICONTENTS_TXT-RUBEMPTY", "Cette rubrique est vide.");


?>
