
        
        <link rel="stylesheet" type="text/css" href="modules/slider/css/style.css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css' />
        <noscript>
            <link rel="stylesheet" type="text/css" href="modules/slider/css/noscript.css" />
        </noscript>

            <div class="wrapper">
                <div id="ei-slider" class="ei-slider">
                    <ul class="ei-slider-large">
                        <li>
                            <img src="modules/slider/images/large/1.jpg" alt="image01" />
                            <div class="ei-title">
                                <h2>Appartemment luxueux</br> en bord de mer</h2>
                                <h3>Cannes</h3>
                            </div>                            
                        </li>
                        <li>
                            <img src="modules/slider/images/large/2.jpg" alt="image02" />
                            <div class="ei-title">
                                <h2>Loft très spacieux</br>equipé haut de gamme</h2>
                                <h3>Dijon</h3>
                            </div>
                        </li>
                        <li>
                            <img src="modules/slider/images/large/3.jpg" alt="image03"/>
                            <div class="ei-title">
                                <h2>Un grand espace<br>en toute tranquillité</h2>
                                <h3>Messigny et vantoux</h3>
                            </div>
                        </li>                        <li>
                            <img src="modules/slider/images/large/1.jpg" alt="image01" />
                            <div class="ei-title">
                                <h2>Appartemment luxueux</br> en bord de mer</h2>
                                <h3>Cannes</h3>
                            </div>                            
                        </li>
                        <li>
                            <img src="modules/slider/images/large/2.jpg" alt="image02" />
                            <div class="ei-title">
                                <h2>Loft très spacieux</br>equipé haut de gamme</h2>
                                <h3>Dijon</h3>
                            </div>
                        </li>
                        <li>
                            <img src="modules/slider/images/large/3.jpg" alt="image03"/>
                            <div class="ei-title">
                                <h2>Un grand espace<br>en toute tranquillité</h2>
                                <h3>Messigny et vantoux</h3>
                            </div>
                        </li>
                    </ul><!-- ei-slider-large -->
                    <ul class="ei-slider-thumbs">
                        <li class="ei-slider-element">Current</li>                       
                        <li><a href="#">Slide 1</a><img src="modules/slider/images/thumbs/1.jpg" alt="thumb01" /></li>
                        <li><a href="#">Slide 2</a><img src="modules/slider/images/thumbs/2.jpg" alt="thumb02" /></li>
                        <li><a href="#">Slide 3</a><img src="modules/slider/images/thumbs/3.jpg" alt="thumb03" /></li>
                        <li><a href="#">Slide 1</a><img src="modules/slider/images/thumbs/1.jpg" alt="thumb01" /></li>
                        <li><a href="#">Slide 2</a><img src="modules/slider/images/thumbs/2.jpg" alt="thumb02" /></li>
                        <li><a href="#">Slide 3</a><img src="modules/slider/images/thumbs/3.jpg" alt="thumb03" /></li>
                    </ul><!-- ei-slider-thumbs -->
                </div><!-- ei-slider -->

            </div><!-- wrapper -->

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
        <script type="text/javascript" src="modules/slider/js/jquery.eislideshow.js"></script>
        <script type="text/javascript" src="modules/slider/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#ei-slider').eislideshow({
                    animation           : 'center',
                    autoplay            : true,
                    slideshow_interval  : 4000,
                    titlesFactor        : 0
                });
            });
        </script>
