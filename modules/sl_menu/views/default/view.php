<?php
/**
* @package		SLASH-CMS
* @subpackage	SL_MENU
* @internal     Menu module
* @version		sl_menu_view.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sl_menu_view extends slView implements iView{


	public function header () {
		
		//<link rel="stylesheet" type="text/css" href="css/superfish.css" media="screen">
		//echo "<link rel='stylesheet' type='text/css' href='core/javascript/jquery_plugins/superfish/css/superfish.css' media='screen'>";
		
		//echo "<link rel='stylesheet' type='text/css' href='admin/templates/system/css/sla_menu.css' media='screen'>";
		// A CHANGER -> POINTER LE STYLE DIRECTEMENT DANS LE TEMPLATE
		
		
		/*
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/superfish/js/hoverintent.js'></script> \n";
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/superfish/js/superfish.js'></script> \n";
		*/
	}
	
	
	public function start_main_menu() {
		echo "<ul id='sl_menu' class='sf-menu'> \n";
	}
	
	public function end_main_menu() {
		echo "</ul> \n";
	}
	
	public function start_menu($title,$type,$action) {
		
		switch ($type) {
			case "url_self" :
				echo "<li class='current'><a href='".$action."'>".$title."</a> \n";
			break;
			case "url_blank" :
				echo "<li class='current'><a href='".$action."' target='_blank'>".$title."</a> \n";
			break;
			case "none":
				echo "<li class='current'><a href='#'>".$title."</a> \n";
			break;
			
			default:
				echo "<li class='current'><a href='#'>".$title."</a> \n";
		}
		
	}
	
	public function end_menu () {
		echo "</li> \n";
	}
	
	public function start_under_menu () {
		echo "<ul> \n";
	}
	
	public function end_under_menu() {
		echo "</ul></li> \n";
	}
	
	
	
	public function execute_menu() {
		
		/*echo "
		<script type='text/javascript'> 
 
			$(document).ready(function(){ 
			$('#sl_menu').superfish({ 
            delay:       1000,                            // one second delay on mouseout 
            animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
            speed:       300,                          // faster animation speed 
            autoArrows:  true,                           // disable generation of arrow mark-up 
            dropShadows: false                            // disable drop shadows 
			}); 
		}); 
 
		</script>";*/
	
	}
	
}





?>