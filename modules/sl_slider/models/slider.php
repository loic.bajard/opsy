<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_slider
* @internal     FRONT Menu module
* @version		slider.php - Version 12.2.14
* @author		Alexis CORNELISSENS
* @copyright	Copyright(C) 2013 - Today. All rights reserved.
* @license		GNU/GPL
*/


class slider extends slModel implements iModel {

	/**
	* Loading menu function
	* @param $parent parent id
	*/
	public function load_slider() {
		$dateJ = date('Y')."-".date('m')."-".date('j')." ".date('H').":".date('i').":00";
		$moduleSlider = $this->slash->sl_module_id("sla_slider","admin");
		
		$result = mysql_query("SELECT * FROM sl_slider WHERE datein<='".$dateJ."' AND (dateout>='".$dateJ."' or dateout='0000-00-00 00:00:00') AND enabled='1' ",$this->slash->db_handle) or 
				$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());

		
		
		if( mysql_num_rows($result)  > 0 ) {
			
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			
				$resultAttachements = mysql_query("SELECT * FROM sl_attachments 
					WHERE id_module='".$moduleSlider."' AND id_element='".$row['id']."' AND state='1' ORDER BY position limit 1",$this->slash->db_handle) or 
					$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if( mysql_num_rows($resultAttachements)  > 0 ) {
					$rowAttachements = mysql_fetch_array($resultAttachements, MYSQL_ASSOC);
				}
			
				$this->controller->view->start_slider($row["id"],$row["title"],$row["content"],$rowAttachements["filename"] );
				
			}
			
			
		} else {
			$this->controller->view->end_slider();
		}
		
		$this->controller->view->end_slider();
	}
	
		public function load_slider_specific($id) {

		$moduleSlider = $this->slash->sl_module_id("sla_slider","admin");
		
		$result = mysql_query("SELECT * FROM sl_slider WHERE id='".$id."'",$this->slash->db_handle) or 
				$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if( mysql_num_rows($result)  > 0 ) {
			
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			
				$resultAttachements = mysql_query("SELECT * FROM sl_attachments 
					WHERE id_module='".$moduleSlider."' AND id_element='".$row['id']."' AND state='1' ORDER BY position limit 1",$this->slash->db_handle) or 
					$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if( mysql_num_rows($resultAttachements)  > 0 ) {
					$rowAttachements = mysql_fetch_array($resultAttachements, MYSQL_ASSOC);
				}
			
				$this->controller->view->one_slider($row["id"],$row["title"],$row["content"],$rowAttachements["filename"] );
				
			}
			
			
		} else {
			$this->controller->view->end_slider();
		}
		
		$this->controller->view->end_slider();
	}



	public function load_slider_all()
	{
		$moduleSlider = $this->slash->sl_module_id("sla_slider","admin");
		
		$result = mysql_query("SELECT * FROM sl_slider",$this->slash->db_handle) or 
				$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if( mysql_num_rows($result)  > 0 ) {
			
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			
				$resultAttachements = mysql_query("SELECT * FROM sl_attachments 
					WHERE id_module='".$moduleSlider."' AND id_element='".$row['id']."' AND state='1' ORDER BY position limit 1",$this->slash->db_handle) or 
					$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if( mysql_num_rows($resultAttachements)  > 0 ) {
					$rowAttachements = mysql_fetch_array($resultAttachements, MYSQL_ASSOC);
				}
			
				$this->controller->view->one_slider($row["id"],$row["title"],$row["content"],$rowAttachements["filename"] );
				
			}
			
			
		} else {
			$this->controller->view->end_slider();
		}
		
		$this->controller->view->end_slider();
	}

	
}

?>