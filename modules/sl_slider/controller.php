 <?php
/**
* @package		SLASH-CMS
* @subpackage	sl_slider
* @internal     Menu module
* @version		sl_slider.php - Version 1
* @author		Alexis CORNELISSENS
* @copyright	Copyright(C) 2013 - Today. All rights reserved.
* @license		GNU/GPL
*/

// Include views
include ("views/default/view.php");
// Include models
include ("models/slider.php");

class sl_slider_controller extends slController implements iController{

	public $module_name = "sl_slider";
	
	public $slider;
	public $view;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function sl_construct() {
       
	   
	   $this->slider = new slider($this);
	   $this->view = new sl_slider_view($this);
	   
	}
	
	
	
	/**
	* Initialise function
	*/
	public function initialise() {
		
		
		
		$this->view->header(); //show script header
		
		//echo $this->slash->sl_config("global_keywords");
		//echo $this->test;
	}
	
	/**
	* Load function
	*/
	public function load() {
		
		
		if ($this->params == "widget" ) {
			$this->view->start_main_slider();
			$this->slider->load_slider();
			$this->view->end_main_slider();
		}else{
			if(isset($this->slash->get_params["id"])){
				$this->slider->load_slider_specific($this->slash->get_params["id"]);
			}
			else{
				$this->slider->load_slider_all();
			}

			
		}
		
		
	}
	
	
	
	
	/**
	* Execute function
	*/
	public function execute() {
		$this->view->execute_slider();
	}

	

}



?>
