 <?php
/**
* @package		SLASH-CMS
* @subpackage	sl_contact
* @internal     front contact module
* @version		sl_contact.php - Version 13.03.18
* @author		Alexis CORNELISSENS
* @copyright	Copyright(C) 2013 - Today. All rights reserved.
* @license		GNU/GPL

This program is free software : you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

include ("views/default/sl_contact_view.php");
include ("models/sl_contact_model.php");

class sl_contact extends slController implements iController {


	/* --- Variables nécessaires --- */
	public $module_name = "sl_contact"; //Nom du module
	public $model;
	public $view;
	
	/**
	* Contructeur de la classe, permet l'initialisation du module
	*/
	function sl_construct() {
       $this->model = new sl_contact_model($this);
       $this->view = new sl_contact_view($this);
	}
	
	public function load_header(){
		$this->view->header(); //on affiche le header (voir fonction d'affichage sl_contact_view.php)
	}
	
	public function load() 
	{

		// Traitement des données		
			if(isset($_GET["idRes"]) )
			{
				$this->view->show_form($this->model->getCommunes(),$this->model->getResidences($_GET["idRes"]));
			}
			else
			{
				$this->view->show_form($this->model->getCommunes(),$this->model->getResidences());		
			}
			


		
		
	
		
	}
	
	public function load_footer(){
		$this->view->footer(); //on affiche le footer (voir fonction d'affichage sl_search_view.php)
	}
	
	

}

?>
