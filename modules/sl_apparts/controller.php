 <?php
/**
* @package		SLASH-CMS
* @subpackage	sl_apparts
* @internal     Menu module
* @version		sl_apparts.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

// Include views
include ("views/default/view.php");
// Include models
include ("models/apparts.php");

class sl_apparts_controller extends slController implements iController{

	public $module_name = "sl_apparts";
	
	public $apparts;
	public $view;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function sl_construct() {
       
	   
	   $this->apparts = new apparts($this);
	   $this->view = new sl_apparts_view($this);
	   
	}
	
	
	/**
	* Initialise function
	*/
	public function initialise() {	
		$this->view->header(); //show script header
	}
	

	/**
	* Load function
	*/
	public function load() {
		
		
		if ($this->params == "widget" ) {
			$this->view->start_main_apparts();
			$this->apparts->load_apparts();
			$this->view->end_main_apparts();
		}else{
			if(isset($this->slash->get_params["id"])){
				$this->apparts->load_apparts_specific($this->slash->get_params["id"]);
			}
			else{
				//Do nothing
			}
		}
	}
	
	/**
	* Execute function
	*/
	public function execute() {
		$this->view->execute_apparts();
	}

}

?>
