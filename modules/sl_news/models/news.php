<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_newsS
* @internal     FRONT Menu module
* @version		menus.php - Version 12.2.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class news extends slModel implements iModel {

	/**
	* Loading menu function
	* @param $parent parent id
	*/
	public function load_news() {
		$dateJ = date('Y')."-".date('m')."-".date('j')." ".date('H').":".date('i').":00";
		$moduleNews = $this->slash->sl_module_id("sla_news","admin");
		
		$result = mysql_query("SELECT * FROM sl_news WHERE datein<='".$dateJ."' AND (dateout>='".$dateJ."' or dateout='0000-00-00 00:00:00') AND enabled='1' ",$this->slash->db_handle) or 
				$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());

		
		
		if( mysql_num_rows($result)  > 0 ) {
			
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			
				$resultAttachements = mysql_query("SELECT * FROM sl_attachments 
					WHERE id_module='".$moduleNews."' AND id_element='".$row['id']."' AND state='1' ORDER BY position limit 1",$this->slash->db_handle) or 
					$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if( mysql_num_rows($resultAttachements)  > 0 ) {
					$rowAttachements = mysql_fetch_array($resultAttachements, MYSQL_ASSOC);
				}
			
				$this->controller->view->start_news($row["id"],$row["title"],$row["content"],$rowAttachements["filename"] );
				
			}
			
			
		} else {
			$this->controller->view->end_news();
		}
		
		$this->controller->view->end_news();
	}
	
		public function load_news_specific($id) {

		$moduleNews = $this->slash->sl_module_id("sla_news","admin");
		
		$result = mysql_query("SELECT * FROM sl_news WHERE id='".$id."'",$this->slash->db_handle) or 
				$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if( mysql_num_rows($result)  > 0 ) {
			
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			
				$resultAttachements = mysql_query("SELECT * FROM sl_attachments 
					WHERE id_module='".$moduleNews."' AND id_element='".$row['id']."' AND state='1' ORDER BY position limit 1",$this->slash->db_handle) or 
					$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if( mysql_num_rows($resultAttachements)  > 0 ) {
					$rowAttachements = mysql_fetch_array($resultAttachements, MYSQL_ASSOC);
				}
			
				$this->controller->view->one_news($row["id"],$row["title"],$row["content"],$rowAttachements["filename"] );
				
			}
			
			
		} else {
			$this->controller->view->end_news();
		}
		
		$this->controller->view->end_news();
	}



	public function load_news_all()
	{
		$moduleNews = $this->slash->sl_module_id("sla_news","admin");
		
		$result = mysql_query("SELECT * FROM sl_news",$this->slash->db_handle) or 
				$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if( mysql_num_rows($result)  > 0 ) {
			
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			
				$resultAttachements = mysql_query("SELECT * FROM sl_attachments 
					WHERE id_module='".$moduleNews."' AND id_element='".$row['id']."' AND state='1' ORDER BY position limit 1",$this->slash->db_handle) or 
					$this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				if( mysql_num_rows($resultAttachements)  > 0 ) {
					$rowAttachements = mysql_fetch_array($resultAttachements, MYSQL_ASSOC);
				}
			
				$this->controller->view->one_news($row["id"],$row["title"],$row["content"],$rowAttachements["filename"] );
				
			}
			
			
		} else {
			$this->controller->view->end_news();
		}
		
		$this->controller->view->end_news();
	}

	
}

?>