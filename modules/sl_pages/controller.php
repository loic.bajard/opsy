<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_pages
* @internal     pages module
* @version		controller.php - Version 11.5.30
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sl_pages_controller extends slController implements iController{

	
	public $module_name = "sl_pages";
	
	
	public $pages; //model
	public $view; //view
	
	
	public function sl_construct() {
	   
	   $this->load_models();
	   $this->load_views();
	  
	   $this->pages = new pages($this);
	   $this->view = new sl_pages_view($this);
	   
	}
	
	/**
	* Load Views
	*/
	private function load_views(){
		if ($this->slash->mobile == true && $this->slash->mobile->isMobile() == true) {
			include ("views/mobile/view.php");
		}else{
			include ("views/default/view.php");
		}
	}
	
	/**
	* Load Models
	*/
	private function load_models() {
		include ("models/pages.php");
	}
	
	/**
	 * Load header function
	 */
	public function load_header(){
		$this->view->header(); //show script header
	}
	
	
	/**
	 * Load footer function
	 */
	public function load_footer(){
		$this->view->footer_page();
	}
	
	/**
	* Load module function
	* Require function by slash core
	*/
	public function load() {
		
		$row_page = $this->pages->load_page($this->slash->sl_param("id","GET"));
		if (isset($row_page["title"])){
			$this->view->show_page($row_page);
		}else{
			$this->view->show_404();
		}
	}
	
	
	
}



?>
