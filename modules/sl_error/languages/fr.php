<?php
/**
* @package		SLASH-CMS
* @subpackage	FR ERRORS LANGUAGES
* @internal     French errors translate
* @version		sl_error.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SL_ERROR
define("ERROR_404", "La page est introuvable");
define("ERROR_403", "Acc&egrave;s interdit");
define("ERROR_x", "Erreur inconnue");
?>
