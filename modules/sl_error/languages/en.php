<?php
/**
* @package		SLASH-CMS
* @subpackage	EN ERRORS LANGUAGES
* @internal     English errors translate
* @version		sl_error.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//Module SL_ERROR
define("ERROR_404", "Page not found");
define("ERROR_403", "Page not allowed");
define("ERROR_x", "Error");
?>
